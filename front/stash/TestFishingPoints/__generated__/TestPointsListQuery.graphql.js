/**
 * @flow
 * @relayHash 4780797b24a52b9a53de3962c7a4ba71
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type TestPointsListItem_point$ref = any;
export type TestPointsListQueryVariables = {|
  count: number,
  after: string,
|};
export type TestPointsListQueryResponse = {|
  +allTrusted: ?{|
    +edges: $ReadOnlyArray<?{|
      +node: ?{|
        +$fragmentRefs: TestPointsListItem_point$ref,
      |},
    |}>,
    +pageInfo: {|
      +hasNextPage: boolean,
      +endCursor: ?string,
    |},
  |},
|};
*/


/*
query TestPointsListQuery(
  $count: Int!
  $after: String!
) {
  allTrusted(first: $count, after: $after) {
    edges {
      node {
        ...TestPointsListItem_point
        id
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}

fragment TestPointsListItem_point on TrustedNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "count",
    "type": "Int!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "cursor",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasNextPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "endCursor",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "TestPointsListQuery",
  "id": null,
  "text": "query TestPointsListQuery(\n  $count: Int!\n  $after: String!\n) {\n  allTrusted(first: $count, after: $after) {\n    edges {\n      node {\n        ...TestPointsListItem_point\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n\nfragment TestPointsListItem_point on TrustedNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {
    "connection": [
      {
        "count": "count",
        "cursor": "after",
        "direction": "forward",
        "path": [
          "allTrusted"
        ]
      }
    ]
  },
  "fragment": {
    "kind": "Fragment",
    "name": "TestPointsListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": "allTrusted",
        "name": "__TestPoints_allTrusted_connection",
        "storageKey": null,
        "args": null,
        "concreteType": "TrustedNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "TrustedNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "TrustedNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "FragmentSpread",
                    "name": "TestPointsListItem_point",
                    "args": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "TestPointsListQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "allTrusted",
        "storageKey": null,
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Variable",
            "name": "first",
            "variableName": "count",
            "type": "Int"
          }
        ],
        "concreteType": "TrustedNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "TrustedNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "TrustedNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "id",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lat",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lon",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "datetime",
                    "args": null,
                    "storageKey": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      },
      {
        "kind": "LinkedHandle",
        "alias": null,
        "name": "allTrusted",
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Variable",
            "name": "first",
            "variableName": "count",
            "type": "Int"
          }
        ],
        "handle": "connection",
        "key": "TestPoints_allTrusted",
        "filters": []
      }
    ]
  }
};
})();
(node/*: any*/).hash = '915c6d0dbe05ac7d0a816cec0e923115';
module.exports = node;
