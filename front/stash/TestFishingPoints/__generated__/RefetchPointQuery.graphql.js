/**
 * @flow
 * @relayHash 151cb2980e6a9b346eb471a551268e63
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type RefetchPointItem_item$ref = any;
export type RefetchPointQueryVariables = {|
  itemID: string,
|};
export type RefetchPointQueryResponse = {|
  +trusted: ?{|
    +$fragmentRefs: RefetchPointItem_item$ref,
  |},
|};
*/


/*
query RefetchPointQuery(
  $itemID: ID!
) {
  trusted(id: $itemID) {
    ...RefetchPointItem_item
    id
  }
}

fragment RefetchPointItem_item on TrustedNode {
  id
  datetime
  lat
  lon
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "itemID",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "itemID",
    "type": "ID!"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "RefetchPointQuery",
  "id": null,
  "text": "query RefetchPointQuery(\n  $itemID: ID!\n) {\n  trusted(id: $itemID) {\n    ...RefetchPointItem_item\n    id\n  }\n}\n\nfragment RefetchPointItem_item on TrustedNode {\n  id\n  datetime\n  lat\n  lon\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "RefetchPointQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "trusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "RefetchPointItem_item",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "RefetchPointQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "trusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "datetime",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lat",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lon",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = 'c92f33f18bf0704ae7010e6e59ff6999';
module.exports = node;
