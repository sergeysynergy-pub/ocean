/**
 * @flow
 * @relayHash f3a2e16a25d2962ed909899713d9b5a5
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type Point_point$ref = any;
export type PointRendererQueryVariables = {|
  q: string,
|};
export type PointRendererQueryResponse = {|
  +bulkTrusted: ?$ReadOnlyArray<?{|
    +$fragmentRefs: Point_point$ref,
  |}>,
|};
*/


/*
query PointRendererQuery(
  $q: String!
) {
  bulkTrusted(q: $q) {
    ...Point_point
    id
  }
}

fragment Point_point on TrustedNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "q",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "q",
    "variableName": "q",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "PointRendererQuery",
  "id": null,
  "text": "query PointRendererQuery(\n  $q: String!\n) {\n  bulkTrusted(q: $q) {\n    ...Point_point\n    id\n  }\n}\n\nfragment Point_point on TrustedNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "PointRendererQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkTrusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": true,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "Point_point",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PointRendererQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkTrusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lat",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lon",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "datetime",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = 'c7c0734cd7e867d250773ce327afb940';
module.exports = node;
