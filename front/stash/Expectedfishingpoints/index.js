import React, { Component } from 'react'
import { Collapse, Button, Card, CardBody } from 'reactstrap'

import './index.css'
import './YearMonthSelector.css'
import ToggleBar from '../../includes/ToggleBar'
import YearMonthSelector from 'react-year-month-selector'


class Expectedfishingpoints extends Component {
  state = {
    open: true,
    from: {
      year: this.props.year,
      month: this.props.month - 1,
      open: false,
    },
    till: {
      year: this.props.year,
      month: this.props.month - 1,
      open: false,
    },
    collapsed: true,
  }

  handleClick = (entity1, entity2) => {
    let state1 = this.state[entity1]
    state1.open = !state1.open

    let state2 = this.state[entity2]
    state2.open = false

    this.setState({[entity1]: state1, [entity2]: state2})
  }

  handleChange = (entity, year, month) => {
    let state = this.state[entity]
    state.year = year
    state.month = month
    this.setState({[entity]: state})
  }

  handleClose = (entity) => {
    let state = this.state[entity]
    state.open = false
    this.setState({[entity]: state})
  }

  handleCheck = (checked) => {
    this.props._setLayer('layer02', checked)
  }

  handleCollapse = (collapsed) => {
    this.setState({ collapsed: collapsed });
  }

  render = () => {
    return <div className='Expectedfishingpoints'>
      <ToggleBar
        checked={this.props.checked}
        collapsed={this.state.collapsed}
        _handleCheck={this.handleCheck}
        _handleCollapse={this.handleCollapse}
        lable={'Предполагаемые скопления'}
      />
      <Collapse isOpen={!this.state.collapsed}><Card><CardBody>
        <div className='FishingPointsFilter'>
          с {' '} <Button
            color="secondary"
            onClick={() => this.handleClick('from', 'till')}
          >
            {this.state.from.year}-{this.state.from.month + 1}
          </Button>
          {' '} по <Button
            color="secondary"
            onClick={() => this.handleClick('till', 'from')}
          >
            {this.state.till.year}-{this.state.till.month + 1}
          </Button>
        </div>
      </CardBody></Card></Collapse>

      <div className='FromSelector'>
        <YearMonthSelector
          year={this.state.from.year}
          month={this.state.from.month}
          onChange={(year, month)=> this.handleChange('from', year, month)}
          open={this.state.from.open}
          onClose={() => this.handleClose('from')}
        />
      </div>
      {/*
      <div className='TillSelector'>
        <YearMonthSelector
          year={this.state.till.year}
          month={this.state.till.month}
          onChange={(year, month)=> this.handleChange('till', year, month)}
          open={this.state.till.open}
          onClose={() => this.handleClose('till')}
        />
      </div>
      */}
    </div>
  }
}


export default Expectedfishingpoints
