/**
 * @flow
 * @relayHash 0b89ae482db30b272ee601f394b06d4a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ExpectedfishingpointsListItem_point$ref = any;
export type ExpectedfishingpointsListQueryVariables = {|
  count: number,
  after: string,
|};
export type ExpectedfishingpointsListQueryResponse = {|
  +allExpectedfishingpoints: ?{|
    +edges: $ReadOnlyArray<?{|
      +node: ?{|
        +$fragmentRefs: ExpectedfishingpointsListItem_point$ref,
      |},
    |}>,
    +pageInfo: {|
      +hasNextPage: boolean,
      +endCursor: ?string,
    |},
  |},
|};
*/


/*
query ExpectedfishingpointsListQuery(
  $count: Int!
  $after: String!
) {
  allExpectedfishingpoints(first: $count, after: $after) {
    edges {
      node {
        ...ExpectedfishingpointsListItem_point
        id
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}

fragment ExpectedfishingpointsListItem_point on ExpectedfishingpointNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "count",
    "type": "Int!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "cursor",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasNextPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "endCursor",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ExpectedfishingpointsListQuery",
  "id": null,
  "text": "query ExpectedfishingpointsListQuery(\n  $count: Int!\n  $after: String!\n) {\n  allExpectedfishingpoints(first: $count, after: $after) {\n    edges {\n      node {\n        ...ExpectedfishingpointsListItem_point\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n\nfragment ExpectedfishingpointsListItem_point on ExpectedfishingpointNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {
    "connection": [
      {
        "count": "count",
        "cursor": "after",
        "direction": "forward",
        "path": [
          "allExpectedfishingpoints"
        ]
      }
    ]
  },
  "fragment": {
    "kind": "Fragment",
    "name": "ExpectedfishingpointsListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": "allExpectedfishingpoints",
        "name": "__Expectedfishingpoints_allExpectedfishingpoints_connection",
        "storageKey": null,
        "args": null,
        "concreteType": "ExpectedfishingpointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExpectedfishingpointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExpectedfishingpointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "FragmentSpread",
                    "name": "ExpectedfishingpointsListItem_point",
                    "args": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ExpectedfishingpointsListQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "allExpectedfishingpoints",
        "storageKey": null,
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Variable",
            "name": "first",
            "variableName": "count",
            "type": "Int"
          }
        ],
        "concreteType": "ExpectedfishingpointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExpectedfishingpointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExpectedfishingpointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "id",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lat",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lon",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "datetime",
                    "args": null,
                    "storageKey": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      },
      {
        "kind": "LinkedHandle",
        "alias": null,
        "name": "allExpectedfishingpoints",
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Variable",
            "name": "first",
            "variableName": "count",
            "type": "Int"
          }
        ],
        "handle": "connection",
        "key": "Expectedfishingpoints_allExpectedfishingpoints",
        "filters": []
      }
    ]
  }
};
})();
(node/*: any*/).hash = '9c148e8817bdcea0d23f718ef54278f2';
module.exports = node;
