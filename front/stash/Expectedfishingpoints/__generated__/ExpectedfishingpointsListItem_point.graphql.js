/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type ExpectedfishingpointsListItem_point$ref: FragmentReference;
export type ExpectedfishingpointsListItem_point = {|
  +id: string,
  +lat: number,
  +lon: number,
  +datetime: any,
  +$refType: ExpectedfishingpointsListItem_point$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "ExpectedfishingpointsListItem_point",
  "type": "ExpectedfishingpointNode",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lat",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lon",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "datetime",
      "args": null,
      "storageKey": null
    }
  ]
};
(node/*: any*/).hash = '90314fa2146fbf8fa4ab772f6dc587cf';
module.exports = node;
