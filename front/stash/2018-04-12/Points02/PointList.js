import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import {ITEMS_PER_PAGE} from '../../constants'
import Point from './Point'
/* End of import section ***************************************/


const PointListQuery = graphql`
  query PointListQuery {
    allExactFishingPoints(last: 10) @connection(key: "Points_allExactFishingPoints", filters: []) {
      edges {
        node {
          ...Point_point
        }
      }
    }
  }
`
/* End of constants and variables section ****************************/


class PointList extends Component {
  _loadMore() {
    // ... you'll implement this in a bit
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={PointListQuery}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('props', props);
          return <div>
            {props.allExactFishingPoints.edges.map(({node}) => {
              return <Point
                key={node.__id}
                point={node}
              />
            })}
            <div className='flex ml4 mv3 gray'>
              <div className='pointer' onClick={() => this._loadMore()}>More</div>
            </div>
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default PointList
