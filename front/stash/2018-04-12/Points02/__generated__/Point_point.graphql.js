/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type Point_point$ref: FragmentReference;
export type Point_point = {|
  +id: string,
  +lat: number,
  +lon: number,
  +datetime: any,
  +$refType: Point_point$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "Point_point",
  "type": "ExactFishingPointNode",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lat",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lon",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "datetime",
      "args": null,
      "storageKey": null
    }
  ]
};
(node/*: any*/).hash = '9eeb207d60b69a9e55c767ab6ce94750';
module.exports = node;
