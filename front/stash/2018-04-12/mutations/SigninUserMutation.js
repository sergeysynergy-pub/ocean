import {
  commitMutation,
  graphql
} from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
  mutation SigninUserMutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      user {
        id
        token
        username
      }
    }
  }
`

export default (username, password, callback) => {
  const variables = {
    username,
    password,
    clientMutationId: ""
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        const id = response.login.user.id
        const token = response.login.user.token
        const username = response.login.user.username
        callback(id, token, username)
      },
      onError: err => console.error(err),
    },
  )
}
