/**
 * @flow
 * @relayHash 001c4d72c23c2285a2666c5549606f27
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type UpdateWorkerMutationVariables = {|
  input: {
    worker?: ?{
      name: string;
      position?: ?string;
      experience?: ?string;
      description?: ?string;
      link?: ?string;
      order?: ?number;
    };
    id: string;
    clientMutationId?: ?string;
  };
|};
export type UpdateWorkerMutationResponse = {|
  +updateWorker: ?{|
    +updatedWorker: ?{|
      +id: string;
      +name: string;
      +position: ?string;
      +experience: ?string;
      +description: ?string;
      +order: number;
    |};
  |};
|};
*/


/*
mutation UpdateWorkerMutation(
  $input: UpdateWorkerInput!
) {
  updateWorker(input: $input) {
    updatedWorker {
      id
      name
      position
      experience
      description
      order
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "UpdateWorkerInput!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "UpdateWorkerMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input",
            "type": "UpdateWorkerInput!"
          }
        ],
        "concreteType": "UpdateWorkerPayload",
        "name": "updateWorker",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": null,
            "concreteType": "WorkerNode",
            "name": "updatedWorker",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "position",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "experience",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "description",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "order",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "UpdateWorkerMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "UpdateWorkerInput!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "UpdateWorkerMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input",
            "type": "UpdateWorkerInput!"
          }
        ],
        "concreteType": "UpdateWorkerPayload",
        "name": "updateWorker",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": null,
            "concreteType": "WorkerNode",
            "name": "updatedWorker",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "position",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "experience",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "description",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "order",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation UpdateWorkerMutation(\n  $input: UpdateWorkerInput!\n) {\n  updateWorker(input: $input) {\n    updatedWorker {\n      id\n      name\n      position\n      experience\n      description\n      order\n    }\n  }\n}\n"
};

module.exports = batch;
