import {
  commitMutation,
  graphql
} from 'react-relay'

import environment from '../Environment'


const mutation = graphql`
  mutation CreateUserMutation($username: String!,
              $email: String!, $password: String!) {
    createUser(username: $username, password: $password, email: $email) {
      user {
        id
        token
        username
      }
    }

    login(username: $username, password: $password) {
      user {
        id
        token
        username
      }
    }
  }
`
export default (username, email, password, callback) => {
  const variables = {
    username,
    password,
    email,
    clientMutationId: ""
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        const id = response.login.user.id
        const token = response.login.user.token
        const username = response.login.user.username
        callback(id, token, username)
      },
      onError: err => console.error(err),
    },
  )
}
