import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'

import Point from './Point'


class PointList extends Component {
  render() {
      // {props.allExactFishingPoints.edges.map(({node}) => {
    return <div>
      {this.props.viewer.allLinks.edges.map(({node}) => {
        return <Point
          key={node.__id}
          point={node}
        />
      })}
    </div>
  }
}


export default createFragmentContainer(PointList, graphql`
  fragment PointList_viewer on ExactFishingPointNode {
    allPoints(last: 100, orderBy: createdAt_DESC) @connection(key: "PointList_allPoints", filters: []) {
      edges {
        node {
          ...Point_point
        }
      }
    }
  }
`)
