import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import PointsListItem from './PointsListItem'
/* End of import section ***************************************/


const PointsListQuery = graphql`
  query PointsListQuery ($after: String!) {
    allExactFishingPoints(
      first: 30
      after: $after
    ) @connection(
      key: "Points_allExactFishingPoints",
      filters: []
    ) {
      edges {
        node {
          ...PointsListItem_point
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`
/* End of constants and variables section ****************************/


class Points extends Component {
  state = {
    after: "",
  }

  _loadMore(after) {
    console.log('more', after);
    this.setState({after: after})
    // ... you'll implement this in a bit
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={PointsListQuery}
      variables={{
        after: this.state.after,
      }}
      render={({error, props}) => {
        // console.log('props', this.props);
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('hm', props);
          let after = props.allExactFishingPoints.pageInfo.endCursor
          let next = props.allExactFishingPoints.pageInfo.hasNextPage
          if (next) {
            this._loadMore(after)
          }

          return <div>
            <hr />
            > {after}
            <br />
            {next}
            <hr />
            {next?
            <div className='flex ml4 mv3 gray'>
              <div className='pointer' onClick={() =>
                  this._loadMore(after)}
              >More</div>
            </div>
            :
            "no next"
            }

            {props.allExactFishingPoints.edges.map(({node}) => {
              return <PointsListItem
                key={node.__id}
                point={node}
              />
            })}

          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Points
