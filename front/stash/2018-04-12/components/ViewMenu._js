import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Icon from 'react-icons-kit';
import { ic_visibility } from 'react-icons-kit/md/ic_visibility'
import { ic_view_module } from 'react-icons-kit/md/ic_view_module'
import { ic_mode_edit } from 'react-icons-kit/md/ic_mode_edit';
import { ic_sort } from 'react-icons-kit/md/ic_sort'
import { ic_add_circle } from 'react-icons-kit/md/ic_add_circle'
import { ic_delete } from 'react-icons-kit/md/ic_delete';

/* End of import section */

export const MenuItemType = {
  VIEW: 'VIEW',
  EDIT: 'EDIT',
  EDIT_LIST: 'EDIT_LIST',
  CREATE: 'CREATE',
  DELETE: 'DELETE',
}
let { CREATE, VIEW, EDIT, DELETE, VIEW_LIST, EDIT_LIST } = MenuItemType

/* End of constants and variables section */

class ViewMenu extends Component {
  items = [
    {
      type: VIEW_LIST,
      title: 'Просмотр списка',
      icon: ic_view_module,
      action: ''
    },
    {
      type: EDIT_LIST,
      title: 'Редактирование списка',
      icon: ic_sort,
      action: '/edit'
    },
    {type: VIEW, title: 'Просмотр', icon: ic_visibility, action: ''},
    {type: EDIT, title: 'Редактирование', icon: ic_mode_edit, action: '/edit'},
    {type: CREATE, title: 'Добавление', icon: ic_add_circle, action: '/create'},
    {type: DELETE, title: 'Удаление', icon: ic_delete, action: '/delete'},
  ]

  render() {
    let items = this.items.filter(item => {
      let answer = false
      this.props.menu.forEach(menu => {
        if (menu === item.type) {
          answer = true
        }
      })
      return answer
    })

    return(
      <div className='ViewMenu'>
      lol
        {items.map((item, key) =>
          <Icon
            key={key}
            size={32}
            icon={item.icon}
            className='Icon'
            title={item.title}
            onClick={(e) => this.props.history.push(
              this.props.url.base + this.props.url.slug + item.action
            )}
          />
        )}
      </div>
    )
  }
}
ViewMenu.PropTypes = {
  menu: PropTypes.array.isRequired,
}


export default ViewMenu
