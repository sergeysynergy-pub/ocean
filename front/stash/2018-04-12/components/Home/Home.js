import React from 'react'
import { Jumbotron } from 'reactstrap'

import './Home.css';

export class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    return (
      <div className='Content'>
        <Jumbotron>
          Home, sweet home.
        </Jumbotron>
    </div>)
  }
}


export default Home
