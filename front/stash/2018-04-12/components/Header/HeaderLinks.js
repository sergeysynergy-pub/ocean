import React from 'react'
import { ButtonGroup, Button } from 'reactstrap'
import { LinkContainer } from 'react-router-bootstrap'

import './HeaderLinks.css';


const linksData = [
  {link: '/home', title: 'Главная', type: 'success',},
  {link: '/workers', title: 'Сотрудники', type: 'success'},
  {link: '/menuitems', title: 'Меню', type: 'success'},
]

const HeaderLinks = ()=> (
  <div className='HeaderLinks'>
    <div className='Container'>
      <Links data={linksData} />
    </div>
  </div>
)


const Links = ({data})=> (<div>
  <ButtonGroup>
    {data.map((element, key) =>
      <LinkContainer to={element.link} key={key}>
        <Button color={element.type}>{element.title}</Button>
      </LinkContainer>
    )}
  </ButtonGroup>
</div>)


export default HeaderLinks
