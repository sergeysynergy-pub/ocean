import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import './Workers.css'
import environment from '../../Environment'
import { USER_ID } from '../../settings'
import ViewMenu from '../ViewMenu'
import WorkersEdit from './WorkersEdit'
/* End of import section ***/

const WorkersMapEditQuery = graphql`
  query WorkersMapEditQuery($workerId: ID!) {
    worker(id: $workerId) {
      ...WorkersEdit_worker
    }
  }
`
/* End of constants and variables section ***/

class WorkersMapEdit extends Component {
  base = '/' + this.props.match.url.split('/')[1] +
    '/' + this.props.match.url.split('/')[2]

  render() {
    const userId = localStorage.getItem(USER_ID)

    return <QueryRenderer
      environment={environment}
      query={WorkersMapEditQuery}
      variables={{workerId: this.props.match.params.id}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          return <div>{userId &&
            <div className='WorkerMapEdit'>
              <ViewMenu
                history={this.props.history}
                base={this.base}
                menu={['view', 'edit', 'delete']}
              />
              <h1>Редактирование сотрудника</h1>
              <WorkersEdit
                key={props.worker.__id}
                worker={props.worker}
                history={this.props.history}
                base={this.base}
              />
            </div>
          }</div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default WorkersMapEdit
