import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import './Workers.css'
import environment from '../../Environment'
import WorkersListItem from './WorkersListItem'
import { USER_ID } from '../../settings'
import ViewMenu from '../ViewMenu'
/* End of import section ***************************************/


const WorkersListQuery = graphql`
  query WorkersListQuery {
    allWorkers(last: 100) @connection(key: "Worker_allWorkers", filters: []) {
      edges {
        node {
          ...WorkersListItem_worker
        }
      }
    }
  }
`
/* End of constants and variables section ****************************/


class Worker extends Component {
  base = '/' + this.props.match.url.split('/')[1]

  render() {
    // console.log('PROPS', this.props.match.url.split('/')[1]);
    const userId = localStorage.getItem(USER_ID)
    /*
    let url = {
      base: '/' + this.props.history.location.pathname.split("/")[1],
      slug: '',
    }
    let menu = [VIEW_LIST]
    let base = '/worker'
              <Route
                exact path="/worker"
                component={WorkerList}
                allWorkers={props.allWorkers}
                base={this.props.match.url}
              />
    */

    return <QueryRenderer
      environment={environment}
      query={WorkersListQuery}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          return <div>
            {userId &&
              <ViewMenu
                history={this.props.history}
                base={this.base}
                menu={['viewList', 'editList', 'create']}
              />
            }
            {props.allWorkers.edges.map(({node}) => {
              return <WorkersListItem
                key={node.__id}
                base={this.props.match.url}
                worker={node}
              />
            })}
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Worker
