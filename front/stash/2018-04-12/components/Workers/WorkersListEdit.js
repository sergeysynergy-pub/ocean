import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
// import { Link } from 'react-router-dom'
import {SortableContainer, SortableElement, SortableHandle} from 'react-sortable-hoc'


import './Workers.css'
import { USER_ID } from '../../settings'
import { arrayMoveByOrder } from '../../cement'
import environment from '../../Environment'
import ViewMenu from '../ViewMenu'
/* End of import section ***************************************/


const WorkersListEditQuery = graphql`
  query WorkersListEditQuery {
    allWorkers(last: 100) @connection(key: "Worker_allWorkers", filters: []) {
      edges {
        node {
          id
          name
          order
        }
      }
    }
  }
`
/* End of constants and variables section ****************************/


class WorkersListEdit extends Component {
  base = '/' + this.props.match.url.split('/')[1]
  state = {
    data: this.props.data,
  }

  onSortEnd = (oldIndex, newIndex) => {
    let newData = arrayMoveByOrder(this.state.data, oldIndex, newIndex)
    this.props._saveData(newData)
    this.setState({
      data: newData,
    })
  }

  /*
  render() {
    let items = this.state.data.map(worker => worker.name)
    return (
        <SortableComponent
          items={items}
          _onSortEnd={this.onSortEnd}
        />
      </div>
    )
  }
  */
  render() {
    const userId = localStorage.getItem(USER_ID)

    return <QueryRenderer
      environment={environment}
      query={WorkersListEditQuery}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props && userId) {
          let items = props.allWorkers.edges.map(({node}) => node.name)
          console.log('items', props.allWorkers.edges);
          return <div className='WorkerList'>
            <ViewMenu
              history={this.props.history}
              base={this.base}
              menu={['viewList', 'editList', 'create']}
            />
            <h1>Редактирование списка</h1>
            <SortableComponent
              items={items}
              // _onSortEnd={this.onSortEnd}
            />
          </div>
        } else if (!userId) {
          return <div>Недостаточно прав</div>
        }
        return <div>Загрузка данных</div>
      }}
    />
  }
}

class SortableComponent extends Component {
  render() {
    return <SortableList
      lockAxis={'y'}
      items={this.props.items}
      /*
      onSortEnd={({oldIndex, newIndex}) =>
        this.props._onSortEnd(oldIndex, newIndex)
      }
      */
      distance={5}
    />
  }
}

const SortableItem = SortableElement(({value}) => {
  return (
    <li>
      {value}
    </li>
  )
})

const SortableList = SortableContainer(({items}) => {
  return (
    <ul>
      {items.map((value, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          value={value}
        />
      ))}
    </ul>
  )
})

export default WorkersListEdit
