import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import './Workers.css'
import environment from '../../Environment'
import { USER_ID } from '../../settings'
import ViewMenu from '../ViewMenu'
/* End of import section ***/


const WorkersDetailQuery = graphql`
  query WorkersDetailQuery($workerId: ID!) {
    worker(id: $workerId) {
      id
      name
      position
      experience
      description
      link
      order
    }
  }
`
/* End of constants and variables section ****************************/


class WorkerDetail extends Component {
  base = '/' + this.props.match.url.split('/')[1] +
    '/' + this.props.match.url.split('/')[2]

  render() {
    const userId = localStorage.getItem(USER_ID)

    return <QueryRenderer
      environment={environment}
      query={WorkersDetailQuery}
      variables={{workerId: this.props.match.params.id}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          return <div>
            {userId &&
            <ViewMenu
              history={this.props.history}
              base={this.base}
              menu={['view', 'edit', 'delete']}
            />
            }

            <h1>{props.worker.name}</h1>
            <div>Должность: {props.worker.position}</div>
            <div>Опыт: {props.worker.experience}</div>
            <div>Описание: {props.worker.description}</div>

            {userId &&
            <div>
              <hr/>
              <div>Сортировка: {props.worker.order}</div>
              <div>Ссылка: {props.worker.link}</div>
            </div>
            }

          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}

export default WorkerDetail
