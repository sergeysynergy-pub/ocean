import React, { Component } from 'react'
import { Button } from 'reactstrap'

import './Workers.css'
import CreateWorkerMutation from '../../mutations/CreateWorkerMutation'
import { USER_ID } from '../../settings'
import ViewMenu from '../ViewMenu'
import { Text, Textarea } from '../forms'
/* End of import section ***/


class CreateWorker extends Component {
  state = {
    // slug: Math.random().toString(36).substring(7),
    name: '',
    position: '',
    experience: '',
    description: '',
    link: '',
  }
  base = '/' + this.props.match.url.split('/')[1]

  render() {
    // console.log('PROPS CREATE', this.props);
    const userId = localStorage.getItem(USER_ID)

    return <div>{userId &&
      <div className='WorkerCreate'>
        <ViewMenu
          history={this.props.history}
          base={this.base}
          menu={['viewList', 'editList', 'create']}
        />
        <h1>Добавление сотрудника</h1>
        <Text
          label='Фамилия Имя Отчество'
          attribute='name'
          self={this}
        />
        <Text
          label='Должность'
          attribute='position'
          self={this}
        />
        <Text
          label='Опыт'
          attribute='experience'
          self={this}
        />
        <Textarea
          label='Описание'
          attribute='description'
          self={this}
        />
        <Text
          label='Ссылка'
          attribute='link'
          self={this}
        />
        <Button
          color="primary"
          onClick={() => this._createWorker()}
        >
          Добавить
        </Button>
      </div>
    }</div>
  }

  _createWorker = () => {
    const {
      name,
      position,
      experience,
      description,
      link,
    } = this.state
    CreateWorkerMutation(
      name,
      position,
      experience,
      description,
      link,
      () => this.props.history.push(this.base)
    )
  }
}


export default CreateWorker
