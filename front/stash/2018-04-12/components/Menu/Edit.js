import React, { Component } from 'react'
import {
  Input, FormGroup, Button, Label
} from 'reactstrap'

/* End of import section */

class WorkerEdit extends Component {
  state = {
    id: this.props.data.id,
    name: this.props.data.name,
    order: this.props.data.order,
    description: this.props.data.description,
  }

  render() {
    console.log('props.data', this.props.data);
    return (
      <div className='WorkersEdit'>
        <FormGroup>
          <Label>Фамилия Имя Отчество</Label>
          <Input
            type="text"
            value={this.state.name}
            onChange={(e) => this.setState({ name: e.target.value })}
          />
        </FormGroup>
        <FormGroup>
          <Label>Сортировка</Label>
          <Input
            type="number"
            value={this.state.order}
            onChange={(e) => this.setState({ order: e.target.value })}
          />
        </FormGroup>
        <FormGroup>
          <Label>Описание</Label>
          <Input
            type="textarea"
            value={this.state.description}
            onChange={(e) => this.setState({ description: e.target.value })}
          />
        </FormGroup>

        <Button
          color="danger"
          onClick={() => {
            this.props._updateWorker(this.state)
            this.props.history.push(this.props.url.base + this.props.url.slug)
          }}
        >
          Сохранить
        </Button>
      </div>
    )
  }
}


export default WorkerEdit
