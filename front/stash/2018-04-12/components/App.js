import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Jumbotron } from 'reactstrap'

import SideMenu from './Menu/SideMenu'
import HeaderLinks from './Header/HeaderLinks'
import Home from './Home/Home'
import Bottom from './Bottom/Bottom'
import WorkersList from './Workers/WorkersList'
import WorkersListEdit from './Workers/WorkersListEdit'
import WorkersCreate from './Workers/WorkersCreate'
import WorkersDetail from './Workers/WorkersDetail'
import WorkersEdit from './Workers/WorkersMapEdit'
// import Menu from '../containers/MapMenu'
import Login from './Login/Login'
import './App.css'

// menu (≥1280px)
// lg (≥1200px)
// md (≥992px)
// sm (768px ≤ width < 992px)
// xs (<768px)
class App extends Component {
  render() {
    return (
      <div className='App'>
        <table>
          <tbody>
            <tr>
              <td className='MenuTd'>
                {/* <Menu /> */}
                <Route path="/" component={SideMenu}/>
              </td>
              <td className='RightTd'>
                {/*
                */}
                <Route path="/" component={HeaderLinks}/>

                {/* Content ***********************************************/}
                <div className='Content'>
                  <Jumbotron>
                    <Switch>
                      <Route exact path="/" component={Home}/>
                      <Route exact path="/home" component={Home}/>

                      <Route exact path="/workers" component={WorkersList}/>
                      <Route exact path="/workers/editlist"
                        component={WorkersListEdit}/>
                      <Route exact path="/workers/create"
                        component={WorkersCreate}/>
                      <Route exact path="/workers/:id" component={WorkersDetail}/>
                      <Route exact path="/workers/:id/edit" component={WorkersEdit}/>

                    {/*
                      <Route exact path="/menu" component={Menu}/>
                      <Route exact path="/menu/:slug" component={Menu}/>
                      <Route exact path="/menu/:slug/:action" component={Menu}/>
                    */}

                      <Route exact path='/login' component={Login}/>
                    </Switch>
                  </Jumbotron>
                </div>
                {/* Content ***********************************************/}
                {/*
                  <Route exact path="/specialisty" component={WorkerList}/>
                  <Route exact path="/specialisty/edit" component={WorkerList}/>
                  <Route exact path="/specialisty/create" component={WorkerList}/>
                  <Route exact path="/specialisty/:slug" component={WorkerDetail}/>
                  <Route exact path="/specialisty/:slug/edit" component={WorkerDetail}/>

                */}
                  <Bottom />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default App
