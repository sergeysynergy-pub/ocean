export const SMALL = 768
export const MEDIUM = 992
export const PORT = 28002
// lg (≥1200px)
// md (≥992px)
// sm (768px ≤ width < 992px)
// xs (<768px)

/*
if (window.location.hostname === '127.0.0.1') {
  export const FRONT = 'http://127.0.0.1:21026'
  export const BACK = 'http://127.0.0.1:21016'
} else {
  export const FRONT = 'http://ocean.cttgroup.ru:21026'
  export const BACK = 'http://ocean.cttgroup.ru:21016'
}
*/
export const FRONT = 'http://127.0.0.1:21026'
export const BACK = 'http://127.0.0.1:21016'

export const USER_ID = 'graphene-user-id'
export const AUTH_TOKEN = 'graphene-auth-token'
export const USER_NAME = 'graphene-user-name'

export const ViewStatus = {
  DEFAULT: 'DEFAULT',
  EDIT_SORT: 'EDIT_SORT',
  CREATE_WORKER: 'CREATE_WORKER',
}
