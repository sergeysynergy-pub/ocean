import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import fetch from 'node-fetch'
//
// import { THRASHER } from '../../../../constants'
//
// import HullLayer from './Layers/Hull/HullLayer'
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined
//

// <HullLayer visible={this.state.hull} hulls={this.state.hulls} />

export default class HullLayer extends Component {
  static propTypes = {
    visible: PropTypes.bool,
    hulls: PropTypes.array,
  }
  static defaultProps = {
    visible: true,
    hulls: [],
  }

  render = () => {
    console.log('thishulls', this.props.hulls)
    const hulls = this.props.hulls.map(dir =>
      <Dir dir={dir} key={dir.id} />
    )

    return (
      <div>
        {this.props.visible &&
          <div>
            {hulls}
          </div>
        }
      </div>
    )
  }
}

class Dir extends Component {
  render = () => {
    console.log('dir', this.props.dir)
    const item = this.props.dir
    let hulls = []
    hulls = item.metafiles.map(hull =>
      <Hull hull={hull} key={hull.id} />
    )

    return (
      <div>
        {item.visible &&
          { hulls }
        }
      </div>
    )
  }
}

class Hull extends Component {
  static propTypes = {
    hull: PropTypes.object,
  }
  static defaultProps = {
    hull: {},
  }

  render = () => {
    const item = this.props.hull

    return (
      <div>
        {item.visible &&
          <RL.Polygon
            positions={item.hull}
            color="#CC00CC"
          />
        }
      </div>
    )
  }
}
