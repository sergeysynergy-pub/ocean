export function XHRGet (url, callback) {
  const xhr = new XMLHttpRequest()
  xhr.responseType = 'json'
  xhr.open('GET', url, true)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      // Request finished. Do processing here.
      if (xhr.status === 200 || xhr.status === 204) {
        // Got positive response
        // console.log("DATA",xhr.response)
        if (callback && typeof callback === 'function') {
          callback(xhr.response)
        }
      } else {
        // Got error
        console.log(`Error: ${xhr.status}`)
      }
    }
  }
  xhr.send()
}

export function XHRPost (url, attributes, callback) {
  const xhr = new XMLHttpRequest()
  xhr.responseType = 'json'
  xhr.open('POST', url, true)
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
  xhr.onreadystatechange = () => {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      // Request finished. Do processing here.
      if (xhr.status === 200 || xhr.status === 204) {
        // Got positive response
        // console.log("DATA",xhr.response)
        if (callback && typeof callback === 'function') {
          callback(xhr.response)
        }
      } else {
        // Got error
        console.log(`Error: ${xhr.status}`)
      }
    }
  }
  xhr.send(attributes)
}

export function latlngsToWKT (latlngs) {
  let wkt = 'SRID=4326;MULTIPOLYGON('
  latlngs.forEach(polygon => {
    wkt +='('
    polygon.forEach(point => {
      wkt += `${point[0]} ${point[1]}, `
    })
    wkt +=')'
  })
  wkt +=')'
  return wkt
}

export function WKTtoLatLngs (wkt) {
  let mpoly = []
  const latlngs = []
  mpoly = wkt.slice(22, -1).split('))')
  mpoly.forEach((element, key) => {
    const poly = element.slice(4)
    if (poly.length > 0) {
      latlngs.push([])
      poly.split(', ').forEach(element => {
        const point = element.split(' ')
        latlngs[key].push([parseFloat(point[1]), parseFloat(point[0])])
      })
    }
  })

  return latlngs
}

export function getRandomInt (max) {
  return Math.floor(Math.random() * Math.floor(max))
}

export const randColor = () => {
  const colors = {
    1: 'red',
    2: 'blue',
    3: 'green',
    4: 'yellow',
    5: 'purple',
    6: 'gray',
    7: 'orange',
    8: 'navyblue',
    9: '#9933FF',
  }

  return (
    colors[getRandomInt(10)]
  )
}
