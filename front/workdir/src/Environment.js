import {
  Environment, Network, RecordSource, Store,
} from 'relay-runtime'

import { THRASHER } from './constants'

const store = new Store(new RecordSource())

const network = Network.create((operation, variables) => (
  fetch(`${THRASHER}/graphql/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => (
    response.json()
  ))
))

export default new Environment({
  network,
  store,
})
