import React from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
//
import { XHRGet } from '../helpers'
import { BACK } from '../constants'


class UserQueries extends React.Component {
  state = {
    table: { colNumbers: -1, body: [], legend: {} },
  }
  componentDidMount () {
    // Async get queries data
    XHRGet(
      `${BACK}/dump/get/`,
      data => {
        // console.log('DATA', data);
        const table = {
          body: data,
          columns: [{
            id: 'status',
            Header: 'Статус',
            accessor: 'status',
            Cell: props => (
              <span className="number">
                {props.value === '1'?'Обрабатывается':'Выполнен'}
              </span>
            ),
          },
          {
            Header: 'ID',
            accessor: 'id',
          },
          {
            Header: 'Время создания',
            accessor: 'timestamp',
          },
          {
            Header: 'Файл',
            accessor: 'file_name',
            Cell: props => (
              <span className="link">
                <a href={`${BACK}/media/data/json/${props.value}`}>
                  {props.value}
                </a>
              </span>
            ),
          },
          {
            Header: 'Размер',
            accessor: 'file_size',
          },
          {
            Header: 'Время выполнения',
            accessor: 'elapsed_time',
          }],
        }
        this.setState({
          table: { table },
        })
      }
    )
  }

  render () {
    return (
      <div className="UserQueries">
        <h1 className="post-title">Запросы</h1>
        {this.state.table.columns === undefined?(
          <div>Загрузка данных</div>
        ):(
          <ReactTable
            data={this.state.table.body}
            columns={this.state.table.columns}
          />
        )}
      </div>
    )
  }
}


export default UserQueries
