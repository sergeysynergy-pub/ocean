import React, { Component } from 'react'
import styled from 'styled-components'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined

const HEIGHT = window.innerHeight - 42

const StyledMap = styled.div`
  margin: 2px 0 0 300px;
`


export default class Map extends Component {
  state = {
    height: HEIGHT,
  }
  // Center of map poisition
  // center = [40, 80] // NorvSea and Kurils
  center = [70, 20]

  // Define mapbox tile layer
  id = 'sergeysynergy.cigwfojr8008nstlykqyvldtv'
  defaultTileLayerURL = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic2VyZ2V5c3luZXJneSIsImEiOiJjaWd3Zm9qejUwMDlydnFrcmloeTBwOW9vIn0.4JdGQv9pO-z4ncyf-DO4Ow'

  componentDidMount = () => {
    // Adding window resize event listener
    window.addEventListener('resize', this.handleResize)

    // Moving leaflet map controls
    this.map.leafletElement.zoomControl.setPosition('topright')
  }

  handleResize = () => {
    this.setState({
      height: HEIGHT,
    })
  }

  render () {
    const mapStyle = { height: this.state.height }

    return (
      <StyledMap>
        <RL.Map
          ref={Map => { this.map = Map }}
          center={this.center}
          zoom={4}
          style={mapStyle}
        >
          {/* Using mapbox as tile layer */}
          <RL.TileLayer
            url={this.defaultTileLayerURL}
            id={this.id}
          />
          {/* Children layers */}
          {this.props.children}
        </RL.Map>
      </StyledMap>
    )
  }
}
