import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined


export default class Directories extends Component {
  /* Component to render set of directories */
  render = () => {
    const directories = []
    this.props.data.forEach((dir, key) => {
      directories.push(
        <Directory key={key} data={dir.datafiles} />
      )
    })

    return (
      <div>
        {directories}
      </div>
    )
  }
}


class Directory extends Component {
  /* Component to render one directory */
  render = () => {
    const files = []
    this.props.data.forEach((file, key) => {
      // Add file data output if checked flag is true
      if (file.checked) {
        // Convert data string to JSON
        const dataset = JSON.parse(file.dataset)
        // Parse dataset to extract latitude, longitude and value for every row
        const points = Object.keys(dataset).map(key => {
          // Split data string to extract values
          const data = dataset[key].split(' ')
          return {
            // Latitude
            lat: Number(data[0]),
            // Longitude
            lon: Number(data[1]),
            // Color
            color: file.color,
          }
        })
        files.push(
          <File key={key} data={points} />
        )
      }
    })

    return (
      <div>
        {files}
      </div>
    )
  }
}

class File extends Component {
  // Component to render points for one file
  render = () => {
    const points = []
    this.props.data.forEach((point, key) => {
      points.push(
        <Point center={[point.lat, point.lon]} key={key} color={point.color} />
      )
    })

    return (
      <div>
        {points}
      </div>
    )
  }
}


class Point extends Component {
  // Just rendering one point by given center coordinates
  static propTypes = {
    center: PropTypes.array,
  }
  static defaultProps = {
    center: [0, 0],
    color: '#CC00CC',
  }

  render = () => (
    <RL.Circle
      center={this.props.center}
      radius={3}
      color={this.props.color}
    />
  )
}
