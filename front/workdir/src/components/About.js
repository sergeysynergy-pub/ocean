import React from 'react'
//
import about from './about.md'
//
export default class About extends React.Component {
  render () {
    return (
      <div className="About">
        {about}
      </div>
    )
  }
}
