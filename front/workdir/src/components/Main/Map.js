import React, { Component } from 'react'
import PropTypes from 'prop-types'

// import Expected from '../Layers/Expectedfishingpoints/ExpectedfishingpointsList'

const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined
// const L = isBrowser ? require('leaflet') : undefined
// const RLC = isBrowser ? require('react-leaflet-control') : undefined
// const handler = isBrowser ? require('leaflet-path-drag') : undefined
const HEIGHT = window.innerHeight - 84


export default class Map extends Component {
  state = {
    // status: 'init',
    // width: window.innerWidth,
    height: HEIGHT,
  }
  position = [61.505, -0.09]

  // Define mapbox tile layer
  id = 'sergeysynergy.cigwfojr8008nstlykqyvldtv'
  defaultTileLayerURL = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic2VyZ2V5c3luZXJneSIsImEiOiJjaWd3Zm9qejUwMDlydnFrcmloeTBwOW9vIn0.4JdGQv9pO-z4ncyf-DO4Ow'

  componentDidMount = () => {
    // Adding window resize event listener
    window.addEventListener('resize', this.handleResize)

    // Moving leaflet map controls
    this.map.leafletElement.zoomControl.setPosition('topright')
  }

  handleClick = e => {
    // Buble region center changes to the parent component
    this.props.handleStateKeyValueUpdate('regionCenter', e.latlng)
  }
  handleResize = () => {
    this.setState({
      // width: window.innerWidth,
      height: HEIGHT,
    })
  }

  render () {
    const mapStyle = { height: this.state.height }

    return (
      <div className="Map">
        <RL.Map
          ref={Map => { this.map = Map }}
          center={this.position}
          zoom={3}
          style={mapStyle}
          onClick={this.handleClick}
        >
          {/* Using mapbox as tile layer */}
          <RL.TileLayer
            url={this.defaultTileLayerURL}
            id={this.id}
          />

          {/* Children layers */}
          {this.props.children}


          { /* Region defined by user */}
          <Region
            positions={this.props.regionPositions}
            handleStateKeyValueUpdate={this.props.handleStateKeyValueUpdate}
          />

          {/* Expected fishing points locations
          {this.props.layer02 &&
          <Expected />
          } */}

        </RL.Map>
      </div>
    )
  }
}
/*
      <h3>{this.state.status}</h3>

        // Using mapbox as tile layer
        <RL.TileLayer
          url={this.defaultTileLayerURL}
          id={this.id}
        />
        */


class Region extends Component {
  static propTypes = {
    handleStateKeyValueUpdate: PropTypes.func,
  }
  static defaultProps = {
    color: 'blue',
  }

  // Handle Region component dragging
  handleDragEnd = () => {
    // Buble path to parent component
    this.props.handleStateKeyValueUpdate(
      'regionCenter', this.proxy.leafletElement.getCenter())
  }

  render () {
    return (<RL.Polygon
      positions={this.props.positions}
      ref={Polygon => { this.proxy = Polygon }}
      color={this.props.color}
      draggable
      onDragEnd={this.handleDragEnd}
      className="Region"
    />)
  }
}
