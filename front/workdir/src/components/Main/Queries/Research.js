import React from 'react'
import { Button } from 'reactstrap'

import './index.css'
import CollapseBar from '../../includes/HandyBar/CollapseBar'


const HEIGHT = window.innerHeight - 43

export default class Research extends React.Component {
  state = {
    width: 200,
    height: HEIGHT,
    collapsed: true,
  }
  style = {
    height: this.state.height,
    width: this.state.width,
  }

  componentDidMount = () => {
    window.addEventListener('resize', this.handleResize)
  }

  handleResize = () => {
    this.setState({ height: HEIGHT })
  }

  handleCollapse = collapsed => {
    this.setState({ collapsed: { collapsed } })
  }

  render () {
    return (
      <div className="Query">
        <CollapseBar
          collapsed={this.state.collapsed}
          _handleCollapse={this.handleCollapse}
          lable="Аэрофотосъёмка"
        />
        {!this.state.collapsed &&
        <div>
          Заказать аэрофотосъёмку выбранного региона
          <form>
            <Button
              block
              color="primary"
            >
              Отправить запрос
            </Button>
          </form>
        </div>
        }
      </div>
    )
  }
}
