import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
import './DefaultRegion.css'
import { BACK } from '../../../constants'
import { XHRGet, WKTtoLatLngs } from '../../../helpers'
import CheckBar from '../../includes/HandyBar/CheckBar'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined


export class DefaultRegionControls extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    _setLayer: PropTypes.func.isRequired,
    checked: PropTypes.bool,
  }
  static defaultProps = {
    checked: true,
  }
  handleCheck = checked => {
    this.props._setLayer(this.props.name, checked)
  }

  render = () => (
    <div className="ProcessedData">
      <CheckBar
        checked={this.props.checked}
        _handleCheck={this.handleCheck}
        lable="Обработанные данные"
      />
    </div>
  )
}

export default class DefaultRegionLayer extends Component {
  static defaultProps = {
    visible: true,
  }

  state = {
    data: [
      [
        [59.453594, 5.897592],
        [64.412255, -15.743695],
        [77.781198, 15.617894],
        [70.515584, 27.955703],
        [59.453594, 5.897592],
      ],
      [
        [43, 145.933452],
        [43, 151.670932],
        [39, 151.670932],
        [39, 145.933452],
        [43, 145.933452],
      ],
    ],
    // visible: this.props.visible,
  }

  componentDidMount = () => {
    // Asynchronous loading default region data from backend
    XHRGet(
      `${BACK}/ocean/api/regions/default/`,
      data => {
        // Convert data in WKT format to multipolygon
        // and update component state
        this.setState({ data: WKTtoLatLngs(data.mpoly) })
      }
    )
  }

  render = () => (
    <div>
      {this.props.visible &&
      <RL.Polygon
        positions={this.state.data}
        color="lightblue"
      />
    }
    </div>
  )
}
