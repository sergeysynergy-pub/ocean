import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined

class Item extends Component {
  render () {
    return (
      <RL.Circle
        key={this.props.point.id}
        center={[this.props.point.lat, this.props.point.lon]}
        radius={10}
        color="#ffcc00"
      />
    )
  }
}

export default createFragmentContainer(Item, graphql`
  fragment ExpectedItem_point on ExpectedNode {
    id
    lat
    lon
    datetime
  }
`)
