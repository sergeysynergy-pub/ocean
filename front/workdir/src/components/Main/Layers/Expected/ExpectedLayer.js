import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
//
import environment from '../../../../Environment'
import ExpectedItem from './ExpectedItem'


const query = graphql`
  query ExpectedLayerQuery ($q: String!) {
    bulkExpected(q: $q) {
      ...ExpectedItem_point
    }
  }
`


export default class Layer extends Component {
  render () {
    return (
      <QueryRenderer
        environment={environment}
        query={query}
        variables={{
          q: this.props.dateFilter,
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            // console.log('props', props);

            return (
              <div>
                {this.props.visible &&
                  props.bulkExpected.map(node => (
                    <ExpectedItem
                      key={node.__id}
                      point={node}
                    />
                  ))
                }
              </div>
            )
          }
          return <div>Loading</div>
        }}
      />
    )
  }
}
