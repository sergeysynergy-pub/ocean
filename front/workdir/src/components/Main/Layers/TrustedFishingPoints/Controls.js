import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
import './index.css'
import CheckBar from '../../../includes/HandyBar/CheckBar'


export default class TrustedFishingPointsControls extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    _setLayer: PropTypes.func.isRequired,
    checked: PropTypes.bool,
  }
  static defaultProps = {
    checked: true,
  }
  handleCheck = checked => {
    this.props._setLayer(this.props.name, checked)
  }

  render = () => (
    <div className="TrustedFishingPoints">
      <CheckBar
        checked={this.props.checked}
        _handleCheck={this.handleCheck}
        lable="Точные рыбные скопления"
      />
    </div>
  )
}
