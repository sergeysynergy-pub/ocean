import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined


class TrustedPointsListItem extends Component {
  render () {
    return (
      <RL.Circle
        key={this.props.point.id}
        center={[this.props.point.lat, this.props.point.lon]}
        radius={10}
        color="green"
      />
    )
  }
}

export default createFragmentContainer(TrustedPointsListItem, graphql`
  fragment TrustedPointsListItem_point on TrustedNode {
    id
    lat
    lon
    datetime
  }
`)
