import React, { Component } from 'react'
// import PropTypes from 'prop-types'
//
import { Hull } from './HullLayer'


export default class Hulls extends Component {
  render = () => {
    const hulls = []
    this.props.hulls.forEach(dir => {
      // console.log('hull dir', dir.id, dir)
      dir.metafiles.forEach(hull => {
        // console.log('file', dir.id, hull)
        hulls.push(
          <Hull hull={hull} key={`${dir.id}-${hull.id}`} />
        )
      })
    })

    return (
      <div>
        {hulls}
      </div>
    )
  }
}
