import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input, Label } from 'reactstrap'
import Icon from 'react-icons-kit'
import { chevronDown } from 'react-icons-kit/fa/chevronDown'
import { chevronUp } from 'react-icons-kit/fa/chevronUp'

import './index.css'


export default class ComboBar extends Component {
  static defaultProps = {
    checked: true,
    collapsed: true,
  }
  static propTypes = {
    _handleCheck: PropTypes.func.isRequired,
    _handleCollapse: PropTypes.func.isRequired,
    lable: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    collapsed: PropTypes.bool,
  }

  state = {
    checked: this.props.checked,
    collapsed: this.props.collapsed,
  }

  handleChange = () => {
    this.props._handleCheck(!this.state.checked)
    this.setState({ checked: !this.state.checked })
  }

  handleCollapse = () => {
    this.props._handleCollapse(!this.state.collapsed)
    this.setState({ collapsed: !this.state.collapsed })
  }

  render = () => (
    <div className="ComboBar">
      <Label check>
        <Input
          type="checkbox"
          checked={this.state.checked}
          onChange={this.handleChange}
        />
        {' '}{this.props.lable}
      </Label>
      <div className="Icon" onClick={this.handleCollapse}>
        {this.state.collapsed?
          <Icon icon={chevronDown} />
        :
          <Icon icon={chevronUp} />
        }
      </div>
    </div>
  )
}
