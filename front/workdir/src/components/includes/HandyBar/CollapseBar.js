import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Icon from 'react-icons-kit'
import { chevronDown } from 'react-icons-kit/fa/chevronDown'
import { chevronUp } from 'react-icons-kit/fa/chevronUp'

import './index.css'


export default class CollapseBar extends Component {
  static propTypes = {
    _handleCollapse: PropTypes.func.isRequired,
    lable: PropTypes.string.isRequired,
    collapsed: PropTypes.bool,
  }
  static defaultProps = {
    collapsed: true,
  }
  state = {
    collapsed: this.props.collapsed,
  }

  handleCollapse = () => {
    this.props._handleCollapse(!this.state.collapsed)
    this.setState({ collapsed: !this.state.collapsed })
  }

  render = () => (
    <div className="CollapseBar">
      {this.props.lable}
      {/* <div className="Icon" onClick={this.handleCollapse}> */}
      <div className="Icon">
        {this.state.collapsed?
          <Icon icon={chevronDown} />
        :
          <Icon icon={chevronUp} />
        }
      </div>
    </div>
  )
}
