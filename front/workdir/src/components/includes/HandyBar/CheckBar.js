import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input, Label } from 'reactstrap'

import './index.css'


export default class CheckBar extends Component {
  static propTypes = {
    _handleCheck: PropTypes.func.isRequired,
    lable: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    id: PropTypes.number,
  }
  static defaultProps = {
    checked: true,
    id: -1,
  }
  state = {
    checked: this.props.checked,
  }

  handleChange = () => {
    this.props._handleCheck(!this.state.checked, this.props.id)
    this.setState({ checked: !this.state.checked })
  }

  handleCollapse = () => {
    this.props.useCollapse._handleCollapse(!this.state.collapsed)
    this.setState({ collapsed: !this.state.collapsed })
  }

  render = () => (
    <div className="CheckBar">
      <Label check>
        <Input
          type="checkbox"
          checked={this.state.checked}
          onChange={this.handleChange}
        />
        {' '}{this.props.lable}
      </Label>
    </div>
  )
}
