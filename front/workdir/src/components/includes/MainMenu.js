import React from 'react'
import { Link } from 'react-router-dom'
import {
  Navbar,
  Nav,
  NavItem,
} from 'reactstrap'
import styled from 'styled-components'


const StyledMenu = styled.div`
  .title {
    color: #ccccff;
    font-size: 1.0em;
  }
`

const links = [
  // { link: '/queries', title: 'Запросы' },
  // {'link': '/auth', 'title': 'Авторизация'},
  // { link: '/about', title: 'О проекте' },
  // { link: 'https://cttgroup.github.io/oceanhub/', title: 'Github' },
  { link: '/tests', title: 'Тестирование' },
]

export default class MainMenu extends React.Component {
  render () {
    const rowsLinks = []
    links.forEach((prop, key) => {
      let className = ''
      if (typeof prop.className !== 'undefined') {
        className = prop.className
      }
      rowsLinks.push(<MenuLink
        key={key}
        link={prop.link}
        title={prop.title}
        className={className}
                     />)
    })

    return (
      <StyledMenu>
        <Navbar color="dark" light expand="md">
          <span className="title">
            Океан: система сбора и анализа океанографических данных
          </span>
          <Nav className="ml-auto">
            {rowsLinks}
          </Nav>
        </Navbar>
      </StyledMenu>
    )
  }
}

// <Link to="/" >Океан</Link>

class MenuLink extends React.Component {
  render () {
    return (
      <NavItem
        // href={this.props.link}
        className={this.props.className}
      >
        <Link to={this.props.link}>{this.props.title}</Link>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      </NavItem>
    )
  }
}
