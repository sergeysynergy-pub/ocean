#!/bin/bash
if [[ $1 = "dev" ]]; then
  docker-compose up -d
  docker exec -d ocean_thrasher_django python manage.py runserver 0.0.0.0:8000
  docker exec -d ocean_thrasher_django celery -A app flower
  docker exec -d ocean_thrasher_django celery -A app beat
  docker exec -d ocean_thrasher_django celery -A app worker -Q parser -n parser
  docker exec -d ocean_thrasher_django celery -A app worker -Q django -n django
  clear
  docker ps|grep thrasher

  exit 1
fi

if [[ $1 = "test" ]]; then
  # launch docker claster
  docker-compose up -d
  # run dev django web-server
  docker exec -d ocean_thrasher_django python manage.py runserver 0.0.0.0:8000
  # run Flow - web based tool for monitoring and administrating Celery clusters
  docker exec -d ocean_thrasher_django celery -A app flower
  # run celery beat task manager demon
  docker exec -d ocean_thrasher_django celery -A app beat
  # run celery parser tasks demon
  docker exec -d ocean_thrasher_django celery -A app worker -Q parser -n parser
  # run celery django tasks demon
  docker exec -d ocean_thrasher_django celery -A app worker -Q django -n django
  # check of launching execution
  clear
  docker ps|grep thrasher

  exit 1
fi

#################################################################

if [[ $1 = "prod" ]]; then
  clear
  docker-compose -f docker-compose.yml -f ./docker-compose.prod.yml up -d

  exit 1
fi

if [[ $1 = "postgis" ]]; then
  clear
  docker run --name ocean_thrasher_chlorophyll \
    -p 21016:5432 \
    -e POSTGRES_PASSWORD=Passw0rd33 \
    -d camptocamp/postgis
    # -d mdillon/postgis
  docker ps | grep postgis

  # docker run -it --link ocean_thrasher_chlorophyll:postgres --rm postgres \
    # sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'

  exit 1
fi

if [[ $1 = "chlorophyllRe" ]]; then
  clear
  docker stop ocean_thrasher_chlorophyll
  docker rm ocean_thrasher_chlorophyll
  docker rmi ocean_thrasher_chlorophyll
  docker-compose --file docker-dev.yml up -d
fi
if [[ $1 = "chlorophyllExec" ]]; then
  docker exec -it  -u postgres ocean_thrasher_chlorophyll psql

  exit 1
fi


if [[ $1 = "thrashedDjangoRe" ]]; then
  clear
  docker stop ocean_thrasher_django
  docker rm ocean_thrasher_django
  docker rmi ocean_thrasher_django
  docker-compose --file docker-dev.yml up -d
fi
