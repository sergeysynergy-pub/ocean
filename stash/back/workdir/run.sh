#!/bin/bash

if [[ $1 = "django" ]]; then
  ./manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "migrate" ]]; then
  ./manage.py makemigrations && ./manage.py migrate

  exit 1
fi

if [[ $1 = "celery" ]]; then
  celery -A app beat &
  celery -A app worker --loglevel=INFO --concurrency=2

  exit 1
fi

if [[ $1 = "back" ]]; then
  celery -A app beat &
  celery -A app worker --loglevel=INFO --concurrency=2 &
  ./manage.py runserver 0.0.0.0:8000

  exit 1
fi
###################################################################
###################################################################
###################################################################

if [[ $1 = "test" ]]; then
  DJANGO_ENV=test celery -A app worker --loglevel=INFO --concurrency=2
  # DJANGO_ENV=test ./manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "prod" ]]; then
  DJANGO_ENV=production python manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "beat" ]]; then
  # celery -A main beat
  celery -A app beat

  exit 1
fi

if [[ $1 = "celeryParser" ]]; then
  # celery -A main worker --loglevel=info -Q parser
  celery -A app worker --loglevel=info -Q parser

  exit 1
fi

if [[ $1 = "celeryDjango" ]]; then
  celery -A app worker --loglevel=info -Q django
  # cd django && celery -A app worker --loglevel=info

  exit 1
fi
