# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-25 14:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parse', '0015_auto_20170523_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='path',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Путь к файлу для обработки'),
        ),
    ]
