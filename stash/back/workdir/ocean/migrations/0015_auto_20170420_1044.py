# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-20 10:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ocean', '0014_t2m_tcc'),
    ]

    operations = [
        migrations.RenameField(
            model_name='chlorophyll',
            old_name='chlor_a',
            new_name='value',
        ),
    ]
