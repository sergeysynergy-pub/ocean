# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-19 08:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ocean', '0019_characteristic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='characteristic',
            name='model',
            field=models.CharField(default='', max_length=200, unique=True, verbose_name='Модель данных'),
        ),
    ]
