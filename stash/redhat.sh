#!/bin/bash

sudo su
cd /tmp
yum update
yum install wget vim git python-pip -y

wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-8.noarch.rpm
rpm -ivh epel-release-7-8.noarch.rpm
sudo tee /etc/yum.repos.d/docker.repo <<-'EOF'

yum install docker-engine
systemctl enable docker.service
systemctl start docker
usermod -aG docker ec2-user
systemctl enable docker
pip install --upgrade backports.ssl_match_hostname
pip install docker-compose
