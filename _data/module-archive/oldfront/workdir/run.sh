#!/bin/bash

if [[ $1 = "dev" ]]; then
  yarn start

  exit 1
fi

if [[ $1 = "test" ]]; then
  DJANGO_ENV=test python manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "prod" ]]; then
  DJANGO_ENV=production python manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "migrate" ]]; then
  python manage.py makemigrations && python manage.py migrate
  python manage.py graphql_schema --indent 2
  mv schema.json /data/.

  exit 1
fi

if [[ $1 = "schema" ]]; then
  python manage.py graphql_schema --indent 2
  mv schema.json /data/.

  exit 1
fi

if [[ $1 = "celery" ]]; then
  celery -A app beat &
  celery -A app worker --loglevel=info -Q parser &
  celery -A app worker --loglevel=info -Q django

  exit 1
fi

if [[ $1 = "beat" ]]; then
  # celery -A main beat
  celery -A app beat

  exit 1
fi

if [[ $1 = "celeryParser" ]]; then
  # celery -A main worker --loglevel=info -Q parser
  celery -A app worker --loglevel=info -Q parser

  exit 1
fi

if [[ $1 = "celeryDjango" ]]; then
  celery -A app worker --loglevel=info -Q django
  # cd django && celery -A app worker --loglevel=info

  exit 1
fi

if [[ $1 = "thrasher" ]]; then
  celery -A app beat &
  celery -A app worker --loglevel=info -Q parser &
  celery -A app worker --loglevel=info -Q django &
  python manage.py runserver 0.0.0.0:8000

  exit 1
fi
