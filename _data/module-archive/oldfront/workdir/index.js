import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

// import { FRONT } from './components/includes/Cement'
import { MainReact } from './components/MainReact'
// import { MainReact } from './components/ReduxMainReact'
import './css/leaflet.css'
import './css/scss.scss'
// import './css/react-select.css'


ReactDOM.render(
  <BrowserRouter>
    <MainReact />
  </BrowserRouter>,
  document.getElementById('mainReactMount')
)

    // <MainReact />
    // <Route path='/' component={MainReact}/>
// ReactDOM.render(<MainReact />, mainReactMount)

/*
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoApp from './reducers'
import App from './components/App'
// import MainReact from './components/ReduxMainReact'

let store = createStore(todoApp)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
*/
