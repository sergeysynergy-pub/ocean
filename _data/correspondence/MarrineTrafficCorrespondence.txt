Возможно, Вы поможете мне в решении вопросов связанных с использованием вашего сервиса: нас привлекают его возможности в нашей работе по исследованию поведения промысловных пород морских рыб средствами ДЗЗ. В качестве тестовой задачи мы бы хотели узнать возможность получения данных за прошедший период с 01 августа 2015 по 31 октября 2015 по траектории движения всех рыболовецких судов в Норвежском море.
Имеется ли такая возможность в принципе? Платная ли она? Если она платная, хотелось бы понять качественные характеристики выборки: кол-во рыболовецких судов, вошедших в выборку; частота обновления координат судов; формат в котором будут предоставленны данные.


Dear Riza Acuna,

First of all, thank you for your detailed answer.

Perhaps You can help me in solving the questions about the using of your service: we were impressed by its capabilities in our work to study the behavior of marine fish using remote sensing. As a test problem we would like to know the possibility of obtaining data for the last period from 01 August 2015 to 31 October 2015 on the trajectory of all fishing vessels in the Norwegian sea.

Is there such a possibility in principle? The cost of this service or type of tariff plan? If we have to pay, I would like to understand the characteristics of the sample first: the number of fishing vessels in the sample; frequency of updates of the coordinates of ships; the format in which data will be provided.
