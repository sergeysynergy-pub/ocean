"""Bulk copying of geo data to parsing directory"""
import os
import shutil
import time
#
GEOSTORAGE_PATH='./volumes/data/byday'
if not os.path.exists(GEOSTORAGE_PATH):
    print("path not found: %s" % (GEOSTORAGE_PATH))
    exit
PARSING_PATH='./volumes/thrasher/data/parsing'
if not os.path.exists(PARSING_PATH):
    print("path not found: %s" % (PARSING_PATH))
    exit

year = '2016'
month = '09'
workdir = '%s/%s/%s' % (GEOSTORAGE_PATH, year, month)
tag = 'OC'

for f in os.listdir(workdir):
    path = os.path.join(workdir, f)
    if os.path.isdir(path):
        for tagDir in os.listdir(path):
            if tagDir == tag:
                tagDirPath = os.path.join(path, tagDir)
                # Wait for possible previouse files operations
                print('tag dir %s' %(tagDirPath))
                time.sleep(10)
                for fyle in os.listdir(tagDirPath):
                    pass
                    shutil.copy('%s/%s' % (tagDirPath, fyle), '%s/buffer' % (PARSING_PATH))
                # Waiting for poissible disk cache operations
                time.sleep(2)
                # Move copied data for parsing
                for fyle in os.listdir('%s/buffer' % (PARSING_PATH)):
                    inFile = os.path.join('%s/buffer' % (PARSING_PATH), fyle)
                    outFile = os.path.join(PARSING_PATH, fyle)
                    # print('in: %s; out: %s' % (inFile, outFile))
                    os.rename(inFile, outFile)
