import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '(se*&d*l$)7f(Er0=W&(8-_@qBu3mw#0w4oow_(!az7l$p)7u#'
# DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'prod.sqlite3'),
    }
}

ALLOWED_HOSTS = ['127.0.0.1', 'ocean', 'ocean.cttgroup.ru', 'arctic.ocean.dev16.int']
