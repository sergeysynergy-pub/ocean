"""Control module for all tasks..."""
from __future__ import absolute_import, unicode_literals
import os
import shutil
from datetime import datetime
from celery import shared_task, chain, chord, group
# from celery.utils.log import get_logger
import logging
import time
#
# from app import celeryApp
from app.settings import PARSING_DIR, CHUNK_SIZE, Cache
from app.helpers import fromBytesToList, getFileNameExt, getNetCDFMetaData
from app.helpers import checkBoundsHit
from app.metaparsers import preCSVMetadata, preNetCDFMetadata, postHullMetadata
from app.metaparsers import preHullMetadata
from app.tasks import createDataPointsTask
from app.parsenetcdf import parseNetCDF
#
# from chlorophyll.tasks import parseAndInsertChlorophyll
# from sst.tasks import parseAndInsertSST
# from kamikadze.tasks import parseAndInsertKamikadze
from trusted.tasks import parseTrusted
from ocean.models import Region
#
# logger = get_logger(__name__)
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)
#
print("DISPATCHER TASKS")


@shared_task
def insertDataToDBCluster(result):
    """Insert results from dataset at Memcached server to database cluster"""
    # logger.info('\n\ninsert result %s\n\n' % (result))

    # time.sleep(4)
    # print('insert result %s' % (result))
    # return result

    # Read data from Memcached server
    dataset = fromBytesToList(Cache.get(result["pointer"]))
    # print("DATASET %s" % (dataset))
    # Number of parsed points
    points = result["points"]
    # Django data model
    model = result["model"]

    # Insert data from dataset to database cluster
    start = 0
    while start < points:
        createDataPointsTask.s(
            dataset[start:start+CHUNK_SIZE],
            model,
        ).apply_async(queue='django')
        start += CHUNK_SIZE

    return {"points to insert": points}


@shared_task
def parseFileFinish(result, filepath):
    """Compose datasets for adding parsed values to DB cluster"""
    print('prepare insertion: %s' % (filepath))
    # print("RESULT %s:" % (result))
    # insertGroup = []
    for characteristic in result:
        print("characteristic %s" % (characteristic))
        return False
        if characteristic['datasets']:
            for dataset in characteristic['datasets']:
                insertDataToDBCluster.s({
                    'model': characteristic['model'],
                    'pointer': dataset['pointer'],
                    'points': dataset['points'],
                }).apply_async(queue='django')
            """
            insertGroup.extend([
                insertDataToDBCluster.s({
                    'model': characteristic['model'],
                    'pointer': dataset['pointer'],
                    'points': dataset['points'],
                }).set(queue='django')
                for dataset in characteristic['datasets']
            ])
            """
    # print("insertGroup %s" % (insertGroup))
    return result


@shared_task
def preParseNetCDF(filepath, tag, model):
    """Common pre parsing function for all NetCDF characteristics"""
    # Desired characteristic tag in filename
    tag = 'SST'
    # Get filename and extension from filepath
    filename, ext = getFileNameExt(filepath)
    # Check if file extension is .nc and filename contains desired tag
    if ext == '.nc' and filename.find(tag) != -1:
        # Get NetCDF metadata
        metadata = getNetCDFMetaData(filepath)
        # Get deafault region of interest form DB
        region = Region.objects.get(pk=1)
        # Check if bounds of sourse NetCDF data fits default regions:
        if checkBoundsHit(region, metadata['boundsWKT']):
            preHullMetadata(metadata['boundsLatLon'], filepath, tag)
            # Return result of NetCDF file parsing
            result = parseNetCDF(filepath, 'sst')
            if result['result']:
                # Add to result Django model name of parsing characteristic
                result['model'] = model
                # Create metafile with hull of parsed points
                # postHullMetadata(result, filepath, tag)
                print("result %s" % (result))
                result['result'] = False
                return result
            else:
                return result
        else:
            return {
                'info': 'Region did not match',
                'datasets': None,
                }
    else:
        return {
            'info': '".nc" file extension and "%s" tag not found' % (tag),
            'datasets': None,
            }


@shared_task
def parseSST(filepath):
    """Preparations needed to parse Sea Sea SurfaceTemperature values"""
    # Desired tag in filename
    tag = 'SST'
    # Django model name of parsing characteristic
    model = 'SeaSurfaceTemperature'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, tag, model)


@shared_task
def parseOC(filepath):
    """Preparations needed to parse Chlorophyll value"""
    # Desired tag in filename
    tag = 'OC'
    # Django model name of parsing characteristic
    model = 'Chlorophyll'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, tag, model)


@shared_task
def parseFile(filepath):
    # All parsers resulting list
    results = []
    # Exctract and append NetCDF data parsing result for 'sst' geo dataset
    print('parsing for SST: %s' % (filepath))
    results.append(parseSST(filepath))
    # Exctract and append NetCDF data parsing result for 'chlor_a' geo dataset
    print('parsing for OC: %s' % (filepath))
    results.append(parseOC(filepath))
    # Return resulting list
    return results

    """
    # Set task group as chord header
    header = chord(
        group(
            # Exctract NetCDF data for given geoset, if poissible
            parseNetCDF.s(fyle, 'sst').set(queue='parser'),
            parseNetCDF.s(fyle, 'chlor_a').set(queue='parser'),
        ),
        group(
            # Insert parsed data from Memcached dataset into DB cluster
            insertDataToDBCluster.s().set(queue='parser'),
            insertDataToDBCluster.s().set(queue='parser'),
        )
    )
    """

    """File parsing flow"""
    # Доделать удаление данных из Memcached!!!

    # Logging:
    # loggerPrefix = '(%s) [parsing file %s%s]# ' % (
    # datetime.now(), workdir, filename)
    # loggerPrefix = '[parsing file %s%s]#' % (workdir, filename)  # 4testing
    # logger.info('%s Start parsing file...' % (loggerPrefix))

    # File path
    filepath = workdir + filename

    # Tasks pipeline
    pipeline = chain()
    # Data types bindings for parsing recognition
    parsers = [
        {
            'tag': 'KDZ', 'ext': 'nc',
            'preMetaParser': preNetCDFMetadata,
            # 'parser': parseAndInsertKamikadze,
            'parser': '',
            'postMetaParser': postHullMetadata,
        },
        {
            'tag': 'OC', 'ext': 'nc',
            'preMetaParser': preNetCDFMetadata,
            'parser': parseAndInsertChlorophyll,
            'postMetaParser': postHullMetadata,
        },
        {
            'tag': 'SST', 'ext': 'nc',
            'preMetaParser': preNetCDFMetadata,
            'parser': parseAndInsertSST,
            'postMetaParser': postHullMetadata,
        },
        {
            'tag': 'TRU', 'ext': 'csv',
            'preMetaParser': preCSVMetadata,
            'parser': parseTrusted,
            'postMetaParser': postHullMetadata,
        },
    ]

    # PARSING FILE:
    # All log enties starts with
    # (datatime) [parsing file workdir/filename]#
    # 1. Start parsing file > log:
    #  # Start parsing file
    # 2. Checking file extension > log:
    #  # Parsing failed: unknown file extension
    # -. Trying to get metadata for given file
    # 3. Checking type tag > log:
    #  # Parsing failed: suitable type tag not found.
    #  # Found suitable parser [parsername]
    #    and added it to the parser tasks set
    # -. Start parallel parsing tasks set

    # Finding suitable file parsers, forming tasks set to execute later:
    for parser in parsers:
        # Looking for suitable extension and type tag in file name for parsing:
        if parser['ext'] == ext[1:] and filename.find(parser['tag']) != -1:
            # logger.info('%s Found suitable parser' % (loggerPrefix))
            # Declare tasks pipeline for execution
            pipeline = chain(
                # Call siutable metadata parser
                parser['preMetaParser'].s(
                    filepath,
                    parser['tag'],
                ).set(queue='parser'),
                # Call siutable parser
                parser['parser'].s(
                    filepath,
                    parser['tag'],
                ).set(queue='parser'),
                # Grouping tasks for parallel execution
                group(
                    # Insert result data to database cluster
                    insertDataToDBCluster.s().set(queue='django'),
                    # Do post parsing metadata processing
                    parser['postMetaParser'].s(
                        filepath,
                        parser['tag'],
                    ).set(queue='parser'),
                )
            ).set(
                queue='parser',
            )

    # Finaly - launch tasks, return execution result
    return pipeline.apply_async()


@shared_task
def parseGeoDataFinish(result, inprogressDir, parsedDir):
    """Finish main parsing process"""
    # Delete parsed files
    for f in os.listdir(inprogressDir):
        fyle = os.path.join(inprogressDir, f)
        if os.path.isfile(fyle):
            ext = os.path.splitext(fyle)[1]  # file extension
            if ext not in ['.log', '.meta']:
                pass
                # os.remove(fyle)

    # Move meta and log files to parsed/timeStamp directory
    os.rename(inprogressDir, parsedDir)

    print('JOB FINISHED! Dir: %s\nresult: %s' % (inprogressDir, result))
    # logger.info('Finished parseGeoData for dir %s' % (inprogressDir))


@shared_task
def parseGeoData():
    """Entry point for parsing tasks: parsing files in PARSING_DIR"""
    logger.info('# || parseGeoData')
    timeStamp = str(datetime.now()).replace(' ', '')  # task run time stamp
    inprogressDir = PARSING_DIR + '/inprogress/' + timeStamp + '/'
    parsedDir = PARSING_DIR + '/parsed/' + timeStamp + '/'

    # Move all files (if any) from PARSING_DIR to inprogress/timeStamp
    # this step is need to prevent file missing or collision
    for f in os.listdir(PARSING_DIR):
        filepath = os.path.join(PARSING_DIR, f)
        if os.path.isfile(filepath):
            if not os.path.exists(inprogressDir):
                os.makedirs(inprogressDir)
            # os.rename(filepath, inprogressDir + f)
            shutil.copy(filepath, inprogressDir + f)  # 4testing

    if os.path.exists(inprogressDir):
        # Group of tasks
        filesGroup = []
        # Grouping file parsing tasks for parallel execution
        for f in os.listdir(inprogressDir):
            filepath = os.path.join(inprogressDir, f)
            # If file found, add parse file task to the tasks group:
            if os.path.isfile(filepath):
                # parseFile(filepath, inprogressDir, f)  # 4 testing
                # Append file parse task to group
                filesGroup.append(
                    chain(
                        parseFile.s(
                            filepath,
                            ).set(queue='parser'),
                        parseFileFinish.s(filepath).set(queue='parser'),
                    )
                )
        # Set task group as chord header
        header = group(filesGroup)
        # Declare callback that only can be executed
        # after all of the tasks in the header have returned
        callback = parseGeoDataFinish.s(
            inprogressDir,
            parsedDir,
            ).set(queue='parser')
        # Finaly launching tasks execution, call callback in the end
        return chord(header)(callback)
    else:
        return None
