"""Parse geodata from files and insert to DB cluster"""
from __future__ import absolute_import, unicode_literals
import os
# import shutil
import numpy as np
from datetime import datetime
from celery import shared_task, chain, chord, group
from django.contrib.gis.geos import Point
# from celery.utils.log import get_logger
import logging
#
# from app import celeryApp
from app.settings import CHUNK_SIZE
from app.settings import INGEST_PARSEANDINSERT
from app.helpers import getFileNameExt, getNetCDFMetaData
from app.helpers import checkBoundsHit, timezonedDateTime
# from app.metaparsers import preCSVMetadata, preNetCDFMetadata
from app.metaparsers import preHullMetadata, postHullMetadata
# from app.tasks import createDataPointsTask
from app.parsenetcdf import parseNetCDF
#
# from chlorophyll.tasks import parseAndInsertChlorophyll
# from sst.tasks import parseAndInsertSST
# from kamikadze.tasks import parseAndInsertKamikadze
# from trusted.tasks import parseTrusted
from ocean.models import Region
from chlorophyll.models import Chlorophyll
from sst.models import SeaSurfaceTemperature
#
# logger = get_logger(__name__)
logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
#
logger.info("PARSEANDINSERT TASKS")


@shared_task
def insertDataToDBCluster(result):
    """Insert results from dataset at Memcached server to database cluster"""
    logger.info('\n\nInserting result to DB cluster: %s\n\n' % (result))

    # Iterates through all parsed data types:
    for dataType in result:
        # If parsed characteristic return result, processed data insertion:
        if dataType['result']:
            # Define convenient variables from result:
            model = dataType['model']
            output = dataType['output']
            points = int(dataType['points'])
            date = dataType['date']

            # Load dataset from file to numpy array
            dataset = np.loadtxt(output)

            # print('val %s' % (dataset[0][2]))
            # Insert data from dataset to database cluster:
            start = 0
            while start < points:
                datasetChunk = [
                    eval(model)(
                        # Set point using lon and lat from numpy array
                        point=Point(row[1], row[0]),
                        # Set value from numpy array
                        value=row[2],
                        # Set datetime using timezone
                        datetime=timezonedDateTime(date),
                    )
                    for row in dataset[start:start+CHUNK_SIZE] \
                    if row[2] != -32767.0
                ]
                # Bulk insert data from dataset to database cluster:
                try:
                    eval(model).objects.bulk_create(datasetChunk)
                except Exception:
                    pass
                # Increase step counter
                start += CHUNK_SIZE

    return result


@shared_task
def preParseNetCDF(filepath, tag, geotag, model):
    """Common pre parsing function for all NetCDF characteristics"""
    # Get filename and extension from filepath
    filename, ext = getFileNameExt(filepath)
    # Check if file extension is .nc and filename contains desired tag
    if ext == '.nc' and filename.find(tag) != -1:
        # Get NetCDF metadata
        metadata = getNetCDFMetaData(filepath)
        # Get deafault region of interest form DB
        region = Region.objects.get(slug='default')
        # Check if bounds of sourse NetCDF data fits default regions:
        if checkBoundsHit(region, metadata['boundsWKT']):
            preHullMetadata(metadata['boundsLatLon'], filepath, tag)
            # Return result of NetCDF file parsing
            result = parseNetCDF(filepath, geotag)
            if result['result']:
                # Add to result Django model name of parsing characteristic
                result['model'] = model
                # Create metafile with hull of parsed points
                postHullMetadata(result, filepath, tag)
                return result
            else:
                return result
        else:
            return {
                'info': 'Region did not match',
                'result': False,
                }
    else:
        return {
            'info': '".nc" file extension and "%s" tag not found' % (tag),
            'result': False,
            }


@shared_task
def parseSST(filepath):
    """Preparations needed to parse Sea Sea SurfaceTemperature values"""
    logger.info('Parsing SST values from file: %s' % (filepath))
    # Desired tag in filename
    tag = 'SST'
    # Desired dataset name in NetCDF file
    geotag = 'sst'
    # Django model name of parsing characteristic
    model = 'SeaSurfaceTemperature'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, tag, geotag, model)


@shared_task
def parseOC(filepath):
    """Preparations needed to parse Chlorophyll value"""
    logger.info('Parsing Chlorophyll values from file: %s' % (filepath))
    # Desired tag in filename
    tag = 'OC'
    # Desired dataset name in NetCDF file
    geotag = 'chlor_a'
    # Django model name of parsing characteristic
    model = 'Chlorophyll'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, tag, geotag, model)


@shared_task
def parseFile(filepath):
    # All parsers resulting list
    results = []
    # Exctract and append NetCDF data parsing result for 'sst' geo dataset
    results.append(parseSST(filepath))
    # Exctract and append NetCDF data parsing result for 'chlor_a' geo dataset
    results.append(parseOC(filepath))
    # Return resulting list
    return results


@shared_task
def parseGeoDataFinish(result, inprogressDir, parsedDir):
    """Finish main parsing process"""
    # Delete parsed files
    for f in os.listdir(inprogressDir):
        fyle = os.path.join(inprogressDir, f)
        if os.path.isfile(fyle):
            ext = os.path.splitext(fyle)[1]  # file extension
            if ext not in ['.log', '.meta']:
                os.remove(fyle)

    # Move meta and log files to parsed/timeStamp directory
    os.rename(inprogressDir, parsedDir)

    print('JOB FINISHED! Dir: %s\nresult: %s' % (inprogressDir, result))
    # logger.info('Finished parsing files in directory: %s' % (inprogressDir))


@shared_task
def parseAndInsert():
    """Entry point for parsing tasks: parsing files in INGEST_PARSEANDINSERT"""
    logger.info('# || parseAndInsert')
    timeStamp = str(datetime.now()).replace(' ', '')  # task run time stamp
    inprogressDir = INGEST_PARSEANDINSERT + '/inprogress/' + timeStamp + '/'
    parsedDir = INGEST_PARSEANDINSERT + '/parsed/' + timeStamp + '/'

    # Move all files (if any) from INGEST_PARSEANDINSERT to inprogress/timeStamp
    # this step is need to prevent file missing or collision
    for f in os.listdir(INGEST_PARSEANDINSERT):
        filepath = os.path.join(INGEST_PARSEANDINSERT, f)
        if os.path.isfile(filepath) or os.path.islink(filepath):
            if not os.path.exists(inprogressDir):
                os.makedirs(inprogressDir)
            os.rename(filepath, inprogressDir + f)
            # shutil.copy(filepath, inprogressDir + f)  # 4testing

    if os.path.exists(inprogressDir):
        # Group of tasks
        filesGroup = []
        # Grouping file parsing tasks for parallel execution
        for f in os.listdir(inprogressDir):
            filepath = os.path.join(inprogressDir, f)
            # If file found, add parse file task to the tasks group:
            if os.path.isfile(filepath) or os.path.islink(filepath):
                # parseFile(filepath, inprogressDir, f)  # 4 testing
                # Append file parse task to group:
                filesGroup.append(
                    chain(
                        parseFile.s(
                            filepath,
                            ).set(queue='parser'),
                        insertDataToDBCluster.s().set(queue='django'),
                        # parseFileFinish.s().set(queue='parser'),
                    )
                )
        # Set task group as chord header
        header = group(filesGroup)
        # Declare callback that only can be executed
        # after all of the tasks in the header have returned:
        callback = parseGeoDataFinish.s(
            inprogressDir,
            parsedDir,
            ).set(queue='parser')
        # Finaly launching tasks execution, call callback in the end
        return chord(header)(callback)
    else:
        return None
