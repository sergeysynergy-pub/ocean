"""Parsing stuctured geodata by: [year]/[month]/[day]/[trait]/[files]"""
from __future__ import absolute_import, unicode_literals
import os
# import shutil
import numpy as np
from datetime import datetime
from celery import shared_task, chain, chord, group
from django.contrib.gis.geos import Point
# from celery.utils.log import get_logger
import logging
#
from app.settings import INGEST_PARSEANDINSERT, CHUNK_SIZE, TESTMODE
from app.helpers import getFileNameExt, getNetCDFMetaData, joinList
from app.helpers import checkBoundsHit, timezonedDateTime, deleteFilesInList
from app.metaparsers import preHullMetadata, postHullMetadata
from app.parsenetcdf import parseNetCDF
from ocean.models import Region
from chlorophyll.models import Chlorophyll
from sst.models import SeaSurfaceTemperature
from .helpers import interpolate
#
# logger = get_logger(__name__)
logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
#
logger.info("PARSEBYDAY TASKS")
PARSING_DIR = INGEST_PARSEANDINSERT


def preParseNetCDF(filepath, path, tag, geotag):
    """Common pre parsing function for all NetCDF characteristics"""
    # Get filename and extension from filepath
    filename, ext = getFileNameExt(filepath)
    # Check if file extension is .nc and filename contains desired tag
    if ext == '.nc' and filename.find(tag) != -1:
        # Get NetCDF metadata
        metadata = getNetCDFMetaData(filepath)
        # Get deafault region of interest form DB
        region = Region.objects.get(slug='default')
        # Check if bounds of sourse NetCDF data fits default regions:
        if checkBoundsHit(region, metadata['boundsWKT']):
            # Return result of NetCDF file parsing
            return parseNetCDF(filepath, geotag, '%s/%s' %
                    (path, filename))
        else:
            return {
                'result': False,
                'info': 'Region did not match',
                }
    else:
        return {
            'result': False,
            'info': '".nc" file extension and "%s" tag not found' % (tag),
            }


def parseSST(filepath, path):
    """Preparations needed to parse Sea Sea SurfaceTemperature values"""
    logger.info('Parsing SST values from file: %s' % (filepath))
    # Desired tag in filename
    tag = 'SST'
    # Desired dataset name in NetCDF file
    geotag = 'sst'
    # Django model name of parsing characteristic
    # model = 'SeaSurfaceTemperature'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, path, tag, geotag)


def parseOC(filepath, path):
    """Preparations needed to parse Chlorophyll value"""
    logger.info('Parsing Chlorophyll values from file: %s' % (filepath))
    # Desired tag in filename
    tag = 'OC'
    # Desired dataset name in NetCDF file
    geotag = 'chlor_a'
    # Django model name of parsing characteristic
    # model = 'Chlorophyll'
    # Exctract NetCDF data for given tag, if poissible
    return preParseNetCDF(filepath, path, tag, geotag)


@shared_task
def parseFile(filepath, path=''):
    """Parsing file for geo data"""
    logger.info('Parsing file %s' % (filepath))
    # All parsers resulting list
    results = []
    # Exctract and append NetCDF data parsing result for 'sst' geo dataset
    results.append(parseSST(filepath, path))
    # Exctract and append NetCDF data parsing result for 'chlor_a' geo dataset
    results.append(parseOC(filepath, path))
    # Return resulting list
    return results


@shared_task
def cookGeoDataFinish(result):
    print('Finish cooking geo data: %s' % (result))
    return result


@shared_task
def cookGeoData():
    """Entry point for parsing tasks: parsing files in PARSING_DIR"""
    inputDir = '%s/input' % (PARSING_DIR)
    # Check if directory exists:
    if not os.path.exists(inputDir):
        return {
            'result': False,
            'info': 'Input directory does not exists'
        }
    # Declare tasks group
    tasksGroup = []
    # Search for files in 'inputDir' for parsing:
    for f in os.listdir(inputDir):
        filepath = os.path.join(inputDir, f)
        if os.path.isfile(filepath):
            # Append file parse task to group:
            tasksGroup.append(
                chain(
                    parseFile.s(
                        filepath,
                        ).set(queue='cooker'),
                    # insertDataToDBCluster.s().set(queue='django'),
                    # parseFileFinish.s().set(queue='parser'),
                )
            )
    # Set task group as chord header
    header = group(tasksGroup)
    # Declare callback that only can be executed
    # after all of the tasks in the header have returned:
    callback = cookGeoDataFinish.s(
        ).set(queue='cooker')
    # Finaly launching tasks execution, call callback in the end
    return chord(header)(callback)


"""
def recurseGeoStorage(path):
    for year in os.listdir(inputDir):
    return result
"""


@shared_task
def parseDirFinish(result, path):
    filesList = []
    # Forming list of successfully parsed files:
    for fyle in result:
        for trait in fyle:
            if (trait['result']):
                filesList.append(trait['output'])
    # Interpolate parsed files results in one file
    if not interpolate(filesList, path):
        return {
            'result': False,
            'info': 'No valid files found'
        }
    deleteFilesInList(filesList)
    # Clear source data files:
    if not TESTMODE:
        deleteFilesInList(filesList)

    return result


@shared_task
def parseByDay():
    """Entry point for parsing tasks: parsing files in PARSING_DIR"""
    inputDir = '%s/input' % (PARSING_DIR)
    # Check if directory exists:
    if not os.path.exists(inputDir):
        return {
            'result': False,
            'info': 'Input directory does not exists'
        }
    # Declare tasks group
    mainTasks = []
    # Hardcoded year-month-day-attribute file parsing:
    for year in os.listdir(inputDir):
        yearPath = os.path.join(inputDir, year)
        if os.path.isdir(yearPath):
            for month in os.listdir(yearPath):
                monthPath = os.path.join(yearPath, month)
                if os.path.isdir(monthPath):
                    for day in os.listdir(monthPath):
                        dayPath = os.path.join(monthPath, day)
                        if os.path.isdir(dayPath):
                            for attribute in os.listdir(dayPath):
                                attributePath = os.path.join(
                                                    dayPath, attribute)
                                if os.path.isdir(attributePath):
                                    # Prepare output path
                                    path = os.path.splitext(attributePath)[0].split('/')
                                    path[2] = 'output'
                                    path = joinList(path, '/')
                                    if not os.path.exists(path):
                                        os.makedirs(path)

                                    # Declare group of tasks
                                    dirTasks = []
                                    # Append files in given directory to group:
                                    for file in os.listdir(attributePath):
                                        filepath = os.path.join(
                                                    attributePath, file)
                                        if os.path.isfile(filepath):
                                            # Append file parse task to group:
                                            dirTasks.append(
                                                parseFile.s(
                                                    filepath,
                                                    path,
                                                    ).set(queue='cooker')
                                                )
                                    # Set task group as chord header
                                    header = group(dirTasks)
                                    # Declare callback for chord header group:
                                    callback = parseDirFinish.s(
                                        path,
                                        ).set(queue='cooker')
                                    # Append chord to main tasks queue
                                    mainTasks.append(chord(header)(callback))

    # Set task group as chord header
    header = group(mainTasks)
    # Declare callback that only can be executed
    # after all of the tasks in the header have returned:
    callback = cookGeoDataFinish.s(
        ).set(queue='cooker')
    # Finaly launching tasks execution, call callback in the end
    # return chord(header)(callback)
    return header
