import numpy as np
from netCDF4 import Dataset
#
from app.datetime import netcdfTimeToDatetimeList
from dispatcher.helpers import interpolateArray
#
#
def processSet():
    """Extract geodata set from NetCDF file, interpolate it to lats/lons
    grids, save results to .csv files
    """
    trait = 'TCC'
    geotag = 'tcc'
    filepath = '/repo/stash/input.tests/wind-tcc.nc'
    root = Dataset(filepath, 'r', format='NETCDF4')
    # print(root,"\n\n")
    lats = root['latitude'][:]
    lons = root['longitude'][:]
    values = root[geotag][:]
    times = root['time'][:]
    day = 0
    # for y in range(lons.shape[0]):

    # For every datetime select numpy array with values and process data:
    # for timeKey in range(times.shape[0]):
    for timeKey in range(6):
        # Declare list of numpy arrays to stack data values
        datasets = []
        # Get values for current time row
        dataset = values[timeKey]
        # Get datetime value in list format from NetCDF time
        datetime = netcdfTimeToDatetimeList(times[timeKey])

        # Proceed only for 00 and 12 hours, otherway skip:
        if datetime[3] != '00' and datetime[3] != '12':
            continue

        print('datetime: %s' % (datetime))
        # for y in range(lons.shape[0]):
        for y in range(6):
            # Create proper longitudes array of lats size
            llons = np.empty(lats.shape[0])
            # Fill it with current longitude row value
            llons.fill(lons[y])
            # Get values for current longitude row
            vals = dataset[:,y]
            # Create data slice numpy array
            data = np.column_stack((lats, llons, vals))
            # Append slice to datasets list
            datasets.append(data)

        # Concatenate all data from dataset list into one numpy array
        arr = np.concatenate((datasets))

        outputPath = '/repo/stash/output.tests/%s/%s' % (timeKey, trait)
        # Need array: lat lon value...
        interpolateArray(arr, outputPath)
