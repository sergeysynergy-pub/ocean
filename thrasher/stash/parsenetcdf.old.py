from __future__ import absolute_import, unicode_literals
import os
from celery import shared_task, group, chain, chord
from celery.utils.log import get_logger
import logging
from netCDF4 import Dataset
import numpy as np
import time
import logging
from osgeo import ogr
#
from app.settings import Cache
from app.helpers import dayToDate
#
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)
#
print("PARSENETCDF")


@shared_task
def parseNetCDFSet(fyle, geotag, start, stop):
    """Async extraction of the geophysical dataset from NetCDF file
    by given start and stop limits"""
    # Array for extracted data from NetCDF file
    data = []
    # Root of NetCDF file
    root = Dataset(fyle, 'r', format='NETCDF4')
    # X axis
    lines = root.dimensions['number_of_lines'].size
    # Y axis
    pixels = root.dimensions['pixels_per_line'].size
    # Latitudes array with values
    lats = root['navigation_data']['latitude']
    # Longitudes array with values
    lons = root['navigation_data']['longitude']
    # Scan time, milliseconds of day
    milliseconds = root['scan_line_attributes']['msec']
    # Desired geophysical dataset
    dataset = root['geophysical_data'][geotag]
    # Number of processed data points
    points = 0

    # Generating pointer to data at Memcached server
    pointer = "%s_%s_%s-%s" % (fyle, geotag, start, stop)
    # Remove possible spacers in pointer name
    pointer = pointer.replace(' ', '_')

    # Setting right value for stop:
    if int(stop) > int(pixels):
        stop = pixels

    # Scan year
    year = int(root['scan_line_attributes']['year'][0])
    # Scan day of year
    day = int(root['scan_line_attributes']['day'][0])
    # Scan date  in Y:m:d format
    date = dayToDate(year, day)

    print('Extracting set: %s' % (pointer))
    # Extract geodata from NetCDF and put it in directory
    for y in range(start, stop):
        for x in range(lines):
            value = dataset[x, y]
            if str(value) != '--':
                try:
                    data.append({
                        "lat": float(lats[x, y]),  # LATITUDE
                        "lon": float(lons[x, y]),  # LONGITUDE
                        "value": float(dataset[x, y]),
                        'datetime': str(date),
                    })
                    points += 1
                except Exception:
                    pass

    # Store data at Memcached server, where:
    # pointer - pointer to data at Memcached server; data - python array
    Cache.set(pointer, data)

    if points > 0:
        return {
            'pointer': pointer,
            'points': points,
            }
    else:
        return None


@shared_task
def parseNetCDF(fyle, geotag):
    """Async extraction of the geophysical dataset from NetCDF file"""
    # Number of lines to extract per extractSet call
    step = 2  # 500 for production
    # Total number of lines to procced
    lines = 4  # 1354 for production
    try:
        result = [
            # Extract set of data from NetCDF
            parseNetCDFSet(fyle, geotag, key, key+step)
            for key in range(0, lines, step)
        ]
        totalPoints = 0
        # Count total number of parsed points in all datasets:
        for row in result:
            totalPoints += row['points']
        return {
            'info': '%s total points have been extracted' % (totalPoints),
            'points': totalPoints,
            'datasets': result,
            }
    except Exception:
        return {
            'info': 'No points have been extracted',
            'datasets': None,
            }
