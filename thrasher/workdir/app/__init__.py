from __future__ import absolute_import, unicode_literals
import logging
import os
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celeryApp
from .getagrids import getaGrids
#
from app.settings import INGEST, OUTPUT, STORAGE
#
#
# logging.basicConfig(level=logging.DEBUG)

__all__ = ['celeryApp']

# Create directories structure for parseAndInsert task
os.makedirs('%s/processfileswithinsert/inprogress' % (INGEST), exist_ok=True)
os.makedirs('%s/processbyset' % (INGEST), exist_ok=True)
os.makedirs('%s/processbyday' % (INGEST), exist_ok=True)

os.makedirs('%s/processfileswithinsert' % (OUTPUT), exist_ok=True)
# os.makedirs('%s/processbyset' % (OUTPUT), exist_ok=True)
# os.makedirs('%s/processbyday' % (OUTPUT), exist_ok=True)

os.makedirs(STORAGE, exist_ok=True)

# Create (if needed) region grids for futher data interpolation
getaGrids()
