from datetime import datetime
from django.utils import timezone


def timezonedDateTime(dateTime):
    """Convert given datetime in string [Y-m-d H:M:S] to timezone,
    aware datetime type
    """
    return timezone.make_aware(
        datetime.strptime(dateTime, '%Y-%m-%dT%H:%M:%S'),
        timezone.get_current_timezone()
    )


def yearMonthStringTodates(str):
    dates = str.split('/')  # split given strings with [year-month] values
    startDate = dates[0] + '-01 00:00:00'  # still string type
    endDate = yearMonthWithLastMonthDay(dates[1]) + ' 00:00:00'  # string type

    return timezonedDateTime(startDate), timezonedDateTime(endDate)
