import graphene

# import users.schema
import ocean.schema
from ocean.schema import TrustedNode, ExpectedNode, SearchResult


class Query(ocean.schema.Query,
            graphene.ObjectType):

    class Meta:
        interfaces = (graphene.relay.Node,)


"""
class Mutation(ocean.schema.Mutation,
                graphene.ObjectType):
    pass
"""


# schema = graphene.Schema(query=Query, mutation=Mutation)
schema = graphene.Schema(
    query=Query,
    # types=[TrustedNode, ExpectedNode, SearchResult],
)
