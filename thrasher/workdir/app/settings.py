import os
from pymemcache.client import base

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$2e+2i!v3dtfr@n88w8=!r2gd4@vo43ny2t0x($2)zu11l5gb0'

# SECURITY WARNING: don't run with debug turned on in production!
# Setting DEBUG flag in depence from environment variable
DEBUG = False
TESTMODE = False


ALLOWED_HOSTS = [
    'ocean.cttgroup.ru',
    '127.0.0.1',
]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',

    # A Django App that adds Cross-Origin Resource Sharing headers to responses
    'corsheaders',
    # GraphQL server implementation framework
    'graphene_django',
    # 'imagekit',

    # Own applications
    'ocean',  # Common purpuse module
    # 'dispatcher',  # Datata extraction and distribution
    'processor',  # Datata extraction and distribution
    'chlorophyll',  # Chlorophyll points processor
    'sst',  # Sea surface temperature processor
    'kamikadze',  # Testing module
    'trusted',  # Trusted fishing points processor
    'beat',  # Celery periodic tasks scheduler
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Needed for django-cors-headers
    'corsheaders.middleware.CorsMiddleware',
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASE_ROUTERS = ('app.dbrouters.DBRouter',)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev_db.sqlite3'),
    },
    'oceandb': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'oceandb',
        'USER': 'postgres',
        'HOST': 'thrasher_oceandb',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
    'chlorophyll': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'chlorophyll',
        'USER': 'postgres',
        'HOST': 'thrasher_chlorophyll',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
    'sst': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'sst',
        'USER': 'postgres',
        'HOST': 'thrasher_sst',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
    'kamikadze': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'kamikadze',
        'USER': 'postgres',
        'HOST': 'thrasher_kamikadze',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
    'trusted': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'trusted',
        'USER': 'postgres',
        'HOST': 'thrasher_trusted',
        'PORT': 5432,
        'PASSWORD': 'Passw0rd33',
    },
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")


# Media settings
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'


# Admin localization settings
# ADMIN_SITE_HEADER = "GraphQL Server"
# ADMIN_SITE_TITLE = "GraphQL Server"
# ADMIN_INDEX_TITLE = "GraphQL Server"


# CORS access settings
CORS_ORIGIN_WHITELIST = (
    '127.0.0.1:3000',
    '127.0.0.1:21010',
    'ocean.cttgroup.ru:21010',
)

# Graphene schem file location
GRAPHENE = {
    'SCHEMA': 'app.schema.schema',
}



# Our custom user model
# AUTH_USER_MODEL = 'users.User'


# Celery config
CELERY_BROKER_URL = 'redis://ocean_thrasher_tasksqueue'
CELERY_RESULT_BACKEND = 'redis://ocean_thrasher_tasksqueue'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'

# Celery beat time rate for checking new data files
BEAT_RATE = 5.0

# Memcached server
Cache = base.Client(('ocean_thrasher_cache', 11211))

# Number of data rows to process in one task
CHUNK_SIZE = 10000  # 10000 the best results compare from 1000 to 100000
# CHUNK_SIZE = 10  # 4testing

# Path to directory with fishing points
FISHING_POINTS_DIR = '/data/fishingpoints'

# Repo paths settings
INGEST = '/repo/ingest'
OUTPUT = '/repo/output'
STORAGE = '/repo/storage'
TESTS = '/repo/tests'
GRIDS = '/repo/grids'

# Try to import local settings from local_settings.py
try:
    from app.local_settings import *
except ImportError:
    pass
