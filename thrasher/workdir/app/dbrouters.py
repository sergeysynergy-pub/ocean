from chlorophyll.models import *
from sst.models import *
from kamikadze.models import *
from trusted.models import *
from ocean.models import *

class DBRouter(object):

    def db_for_read(self, model, **hints):
        """ reading data models from different DB's """
        if model == Chlorophyll:
            return 'chlorophyll'
        if model == SeaSurfaceTemperature:
            return 'sst'
        if model == Kamikadze:
            return 'kamikadze'
        if model == Trusted:
            return 'trusted'
        if model == Region:
            return 'oceandb'
        return None

    def db_for_write(self, model, **hints):
        """ writing data models to different DB's """
        if model == Chlorophyll:
            return 'chlorophyll'
        if model == SeaSurfaceTemperature:
            return 'sst'
        if model == Kamikadze:
            return 'kamikadze'
        if model == Trusted:
            return 'trusted'
        if model == Region:
            return 'oceandb'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'chlorophyll':
            return db == 'chlorophyll'
        if app_label == 'sst':
            return db == 'sst'
        if app_label == 'kamikadze':
            return db == 'kamikadze'
        if app_label == 'trusted':
            return db == 'trusted'
        if app_label == 'ocean':
            return db == 'oceandb'
        return None
