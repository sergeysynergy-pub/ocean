from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.utils import timezone


# Test sea surface temperature model
class Kamikadze(models.Model):
    # Longitude and latitude geo point
    point = models.PointField(default=Point(6, 70))
    # Datetime value
    datetime = models.DateTimeField(default=timezone.now, db_index=True)
    # Sea surface temperature, Kelvin
    value = models.FloatField(
        "Температура поверхности океана, К", null=True)
