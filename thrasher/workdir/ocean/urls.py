from django.urls import path

from . import views


urlpatterns = [
    path('parsefilescheck/', views.parseFileCheck),
    path('quickhulltest/', views.quickHullTest),
    path('testdatapoints/', views.testDataPoints),
]
