import csv
from datetime import datetime

from app.settings import Cache, CHUNK_SIZE


# Parsing csv file for latitudes, longitudes and dates data
def parseCSVandCache(fyle):
    name = (fyle + '-' + str(datetime.now())).replace(' ', '')
    # Define object to hadle .csv file
    reader = csv.DictReader(
        open(fyle, 'r'),
        fieldnames = ['lat', 'lon', 'date'],
        delimiter = '/',
        quotechar = '"'
    )

    # Define list to store data from file
    data = []

    # Define list of memcached sets for futher calculations in Celery task
    cacheSets = []

    # Fill dict array with geo data;
    # store dict at Memcached server for further calculations;
    # create task for further calculations at Redis server.
    counter = 0
    chunk = 0
    for row in reader:
        if row['lat'] != 'lat':
            if row['date']:
                date = row['date'] + ' 00:00:00'
            else:
                date = '2000-01-01 00:00:00'
            data.append({
                'lat': row['lat'],
                'lon': row['lon'],
                'datetime': date,
            })
        counter += 1
        if counter >= CHUNK_SIZE:
            chunk += 1
            setName = name + '-' + str(chunk)
            Cache.set(setName, data)  # store data at Memcached server
            cacheSets.append(setName)
            counter = 0
            data = []

    if counter > 0:
        setName = name + '-' + str(chunk + 1)
        Cache.set(setName, data)  # store data at Memcached server
        cacheSets.append(setName)

    if len(cacheSets) > 0:
        return cacheSets
    else:
        return False
