from django.contrib.gis.db import models
from django.contrib.gis.geos import Polygon, MultiPolygon


class Trusted(models.Model):
    lat = models.FloatField()
    lon = models.FloatField()
    datetime = models.DateTimeField()


class Expected(models.Model):
    lat = models.FloatField()
    lon = models.FloatField()
    datetime = models.DateTimeField()


class Region(models.Model):
    def __str__(self):
        return self.title

    # Norv Sea
    norvSea = Polygon((
        (5.897592, 59.453594),
        (-15.743695, 64.412255),
        (15.617894, 77.781198),
        (27.955703, 70.515584),
        (5.897592, 59.453594),
    ))
    # Norv Sea brutal
    norvSeaBrutal = Polygon((
        (-15.743695, 59.453594),
        (-15.743695, 77.781198),
        (27.955703, 77.781198),
        (27.955703, 59.453594),
        (-15.743695, 59.453594),
    ))
    # Kurils islands
    kurils = Polygon((
        (145.933452, 43.902974),
        (151.670932, 43.902974),
        (151.670932, 39.980028),
        (145.933452, 39.980028),
        (145.933452, 43.902974),
    ))
    mpolygon = MultiPolygon(norvSea, kurils)

    title = models.CharField(
        max_length=50, default='Default')
    slug = models.SlugField(default='default')
    mpoly = models.MultiPolygonField(default=mpolygon)
