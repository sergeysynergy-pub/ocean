import json
import os
import pandas as pd
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import logging
#
from app.settings import OUTPUT
from app.helpers import sortDirsByTime, sortFilesByTime, getFilename
# from .tests import getNetCDFMetaData
#
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)  # 4 testing


# Return latLng polygon of given attribute type for NetCDF file
@require_http_methods(["GET", "POST"])
def parseFileCheck(request):
    # path = '/data/parsing/test/01_SST.nc'
    # path = '/data/parsing/test/72points_OC.nc'
    # ncTag = 'chlor_a'
    # data = getNetCDFMetaData(path)
    data = {}
    result = json.dumps(data, ensure_ascii=False)
    return JsonResponse(result, safe=False)


@require_http_methods(["GET", "POST"])
def quickHullTest(request):
    # Directory with parsed metafiles
    path = '%s/processfileswithinsert' % (OUTPUT)
    # path = '/data/parsing/inprogress/'
    # Rusult array
    data = []
    # Limiting number of directories to fetch for metadata
    limit = 100

    dirs = sortDirsByTime(path, limit)
    for path in dirs:
        dirname = path.split("/")[-1]
        files = sortFilesByTime(path, '.meta')
        metafiles = []
        for metafile in files:
            logger.info('metafile: %s\n' % (metafile))
            try:
                with open(metafile) as jsonData:
                    metadata = json.load(jsonData)
                    # logger.info('metadata: %s\n' % (metadata))
                    metafiles.append({
                        "filename": metadata["filename"],
                        "quickHull": metadata["quickHull"],
                    })
            except Exception:
                pass
        if len(metafiles) > 0:
            data.append({
                "dirname": dirname,
                "metafiles": metafiles,
            })
            metafiles = []

    # result = json.dumps(data, ensure_ascii=False)

    return JsonResponse(data, safe=False)


@require_http_methods(["GET", "POST"])
def testDataPoints(request):
    # Directory with parsed metafiles
    path = '/repo/tests/datapoints'
    # Create directory if needed:
    if not os.path.exists(path):
        os.makedirs(path)
    # Rusulting array
    result = []
    # Limiting number of directories to fetch for metadata
    limit = 100
    # Sort list of directories
    dirs = sortDirsByTime(path, limit)
    # Extract data from files:
    for path in dirs:
        dirname = path.split("/")[-1]
        files = sortFilesByTime(path, '.csv')

        datafiles = []
        for fyle in files:
            # Read data from CSV file using pandas library
            data = pd.read_csv(fyle,  header=None)
            # Convert data to json
            dataset = data.to_json()
            # Crop string for better look up
            dataset = dataset[5:-1]
            # Append file data to output result:
            datafiles.append({
                "filename": getFilename(fyle)['name'],
                "dataset": dataset,
            })

        # Append directory data to the resul list:
        if len(datafiles) > 0:
            result.append({
                "dirname": dirname,
                "datafiles": datafiles,
            })
            datafiles = []

    return JsonResponse(result, safe=False)
