import graphene
from datetime import datetime
from graphene import relay, ObjectType, InputObjectType, Union
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
# import django_filters
from django.core.exceptions import ValidationError
from django_filters import FilterSet
from django.utils import timezone

from ocean.models import Trusted, Expected
from app.datetime import yearMonthStringTodates


class TrustedFilter(FilterSet):
    # datetime__gt = django_filters.NumberFilter(name='datetime', lookup_expr='datetime__gt')
    class Meta:
        model = Trusted
        fields =  {
            'datetime': ['exact', 'year__gt'],
        }
class TrustedNode(DjangoObjectType):
    class Meta:
        model = Trusted
        interfaces = (graphene.relay.Node, )


class ExpectedFilter(FilterSet):
    class Meta:
        model = Expected
        fields = ['datetime']
class ExpectedNode(DjangoObjectType):
    class Meta:
        model = Expected
        interfaces = (graphene.relay.Node, )


class SearchResult(graphene.Union):
    class Meta:
        types = (TrustedNode, ExpectedNode)


class Query(ObjectType):
    node = relay.Node.Field()
    trusted = relay.Node.Field(TrustedNode)
    expected = relay.Node.Field(ExpectedNode)

    allTrusted = \
        DjangoFilterConnectionField(
            TrustedNode,
            filterset_class=TrustedFilter,
        )
    allExpecteds = \
        DjangoFilterConnectionField(
            ExpectedNode,
            filterset_class=ExpectedFilter,
        )


    bulkTrusted = graphene.List(TrustedNode, q=graphene.String())
    def resolve_bulkTrusted(self, info, **kwargs):
        q = kwargs.get("q")  # Search query
        if q:
            # print('Trusted:', q.split('/'))
            startDate, endDate = yearMonthStringTodates(q)

            allTrusted = Trusted.objects.filter(
                datetime__range=[startDate, endDate])
        else:
            allTrusted = Trusted.objects.all()

        return allTrusted


    bulkExpected = graphene.List(ExpectedNode, q=graphene.String())
    def resolve_bulkExpected(self, info, **kwargs):
        q = kwargs.get("q")  # Search query
        if q:
            startDate, endDate = yearMonthStringTodates(q)
            allExpected = Expected.objects.filter(
                datetime__range=[startDate, endDate])
            return allExpected
        else:
            allExpected = Expected.objects.all()


    search = graphene.List(SearchResult, q=graphene.String())
    def resolve_search(self, info, **args):
        # q = args.get("q")  # Search query
        # print('QQQQQ:', q)
        all_trusted = Trusted.objects.all()

        return all_trusted
