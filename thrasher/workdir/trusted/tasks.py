"""
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.utils.log import get_logger
#
from app.parsecsv import parseCSV
# from app.tasks import createDataPointsTask
#
logger = get_logger(__name__)


# Async parsing of given CSV file,
# async inserting parsed data into database cluster
@shared_task
def parseTrusted(metadata, filepath, tag):
    logger.info('|| parseTrusted')
    # Call to parse csv file and store extracted data in cache
    result = parseCSV(filepath)

    if result:
        # Append django data model name to metadata for futher processing
        result["metadata"]["model"] = "Trusted"

        return result

    else:
        return False
"""
