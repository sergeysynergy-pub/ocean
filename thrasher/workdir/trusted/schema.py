import graphene
from datetime import datetime
from graphene import relay, ObjectType, InputObjectType, Union
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from django.core.exceptions import ValidationError
from django_filters import FilterSet
from django.utils import timezone
#
from app.datetime import yearMonthStringTodates
#
from .models import Trusted


class TrustedFilter(FilterSet):
    class Meta:
        model = Trusted
        fields =  {
            'datetime': ['exact', 'year__gt'],
        }
class TrustedNode(DjangoObjectType):
    class Meta:
        model = Trusted
        interfaces = (graphene.relay.Node, )


class SearchResult(graphene.Union):
    class Meta:
        types = (TrustedNode)


class Query(ObjectType):
    node = relay.Node.Field()
    trusted = relay.Node.Field(TrustedNode)

    allTrusted = \
        DjangoFilterConnectionField(
            TrustedNode,
            filterset_class=TrustedFilter,
        )

    bulkTrusted = graphene.List(TrustedNode, q=graphene.String())
    def resolve_bulkTrusted(self, info, **kwargs):
        q = kwargs.get("q")  # Search query
        if q:
            startDate, endDate = yearMonthStringTodates(q)

            allTrusted = Trusted.objects.filter(
                datetime__range=[startDate, endDate])
        else:
            allTrusted = Trusted.objects.all()

        return allTrusted


    search = graphene.List(SearchResult, q=graphene.String())
    def resolve_search(self, info, **args):
        all_trusted = Trusted.objects.all()

        return all_trusted
