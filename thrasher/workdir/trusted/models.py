from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.utils import timezone

# Trusted fishing point
class Trusted(models.Model):
    # Longitude and latitude geo point
    point = models.PointField(default=Point(6, 70))
    # Datetime value
    datetime = models.DateTimeField(default=timezone.now, db_index=True)

    # Trusted fishing point existance: 0 or 1,
    # value added for compatibility with other geo data characteristics
    value = models.FloatField("Точное наличие рыбных скоплений", default=1)
