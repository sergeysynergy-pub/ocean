#!/usr/bin/python
import os
import sys
from scipy.interpolate import griddata
#
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
try:
    import django
except ImportError as exc:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    ) from exc
print('Setup django environment...')
django.setup()  # Setup Django env for testing
print('Done.\nRunning tests...')
#
from osgeo import ogr
from netCDF4 import Dataset
import numpy as np
import numpy.ma as ma
import h5py
from django.contrib.gis.geos import Polygon, MultiPolygon
#
from app.settings import TESTMODE, DEBUG
#from extractor.extractandinsert import extractAndInsert
# from dispatcher.geocooker import cookGeoData, cookGeoDataByDay
# from ocean.models import Region
# from app.parsenetcdf import parseNetCDF
from processor.processbyday import processByDay
from processor.processfileswithinsert import processFilesWithInsert
#
#
print('TESTMODE: %s' % (TESTMODE))
print('DEBUG: %s' % (DEBUG))
#
if __name__ == "__main__":
    """ Processing command line arguments """
    if len(sys.argv) > 1:
        if 'insert' in sys.argv:
            processFilesWithInsert()
        if 'byday' in sys.argv:
            processByDay()
    else:
        """ No arguments """

        processSet()

        # region = Region.objects.get(slug='default')
        # dr = str(region.mpoly)
        # register01 = Region.objects.get(slug='register01')
        # rg = str(register01.mpoly)
        # print('region: %s\n' % (wkt1))
        # poly1 = ogr.CreateGeometryFromWkt(wkt1)

        # filepath = '/parsing/test/72points_OC.nc'
        # filepath = '/parsing/test/T2013275210000.hit.L2_LAC_SST.nc'
        # filepath = '/parsing/input/T2016265105500.L2_LAC_OC.nc'


        """
        # print(parseNetCDF(filepath, 'sst'))
        geotag = 'chlor_a'
        # X axis
        lines = root.dimensions['number_of_lines'].size
        # Y axis
        pixels = root.dimensions['pixels_per_line'].size
        # Latitudes as numpy array
        lats = root['navigation_data']['latitude'][:]
        # Longitudes as numpy array
        lons = root['navigation_data']['longitude'][:]
        # Extract desired geophysical dataset values as numpy array
        dataset = root['geophysical_data'][geotag]
        # print(dataset,"\n\n")
        """

        """
        v = dataset[:]
        # print(v.shape,"\n")

        # print(v.mask)
        # ma.masked_outside(values, 0.1, 0.9)
        # print(v[~v.mask])
        c = v.compressed()
        print(c.shape,"\n")

        mlats = ma.masked_array(lats, mask=v.mask)
        print(mlats.shape,"\n")
        """

        """
        zeros = np.array(['--'])
        # cvals = np.delete(rvals, ['--'])
        cvals = np.setdiff1d(rvals, zeros)
        print(cvals.shape,"\n")
        """

        # dataset = np.loadtxt('/parsing/output/T2016244004500.L2_LAC_OC.result.chlor_a.csv')
        # dataset = np.loadtxt('/parsing/output/5500.result_OC.csv')

        # dataset = np.loadtxt('/parsing/output/5500big.result_OC.csv')

        """
        lats = dataset[0:,0]
        lons = dataset[0:,1]
        vals = dataset[0:,2]
        points = np.column_stack((lats, lons))
        print(points)
        print(vals)

        glats = np.linspace(59.45, 77.78, 1652)
        glons = np.linspace(-15.74, 27.96, 2035)
        vlons, vlats = np.meshgrid(glons, glats)

        grid = griddata(points, vals, (vlons, vlats), method='nearest')
        print('vlats', vlats.shape)
        print('vons', vlons.shape)
        print('grid', grid.shape)
        result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
        np.savetxt('/parsing/output/result_sst_grid.csv', result)
        """
