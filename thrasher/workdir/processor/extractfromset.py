"""Extract geo data from NetCDF files and interpolate to grid"""
import os
import numpy as np
import numpy.ma as ma
from netCDF4 import Dataset
from celery import shared_task, group, chord
import logging
#
from app.settings import TESTMODE, INGEST, TESTS
#
from .datetime import netcdfTimeToDatetimeList


logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
logger.info("PROCESSBYSET TASKS")

INPUTDIR = '%s/processbyset' % INGEST


###########################################################################
# Celery tasks section                                                    #
###########################################################################
@shared_task(queue='processor')
def extractByTrait(filepath, trait, geotag):
    """Extract geodata set from NetCDF file, interpolate it to lats/lons
    grids, save results to .csv files
    """
    logger.info('Processing file by trait: %s' % filepath)
    root = Dataset(filepath, 'r', format='NETCDF4')
    # print(root,"\n\n")
    lats = root['latitude'][:]
    lons = root['longitude'][:]
    # Convert longitude values system from 0:360 to -180:+180
    lons = lons - 180
    values = root[geotag][:]
    times = root['time'][:]

    # List with resulting numpy arrays
    results = []

    # For every datetime select numpy array with values and process data:
    timeRange = 6 if TESTMODE else times.shape[0]
    for timeKey in range(timeRange):
        # List of numpy arrays to stack data values
        datasets = []
        # Get values for current time row
        dataset = values[timeKey]

        # Trying to get numpy mask for dataset numpy array:
        try:
            dataset.mask
            # Rasing warnign in case mask was found
            logger.warning('MASKED DETECTED, need to check results manualy')
        except Exception:
            pass

        # Get datetime value in list format from NetCDF time
        datetime = netcdfTimeToDatetimeList(times[timeKey])

        # Proceed only for 00 and 12 hours, otherway skip:
        if datetime[3] != '00' and datetime[3] != '12':
            continue

        # Data arrays preparation:
        # normalize lon and lat arrays, add values to normalized array
        for y in range(lons.shape[0]):
            # Create proper longitudes array of lats size
            llons = np.empty(lats.shape[0])
            # Fill it with current longitude row value
            llons.fill(lons[y])
            # Get values for current longitude row
            vals = dataset[:, y]
            # Create data slice numpy array
            data = np.column_stack((lats, llons, vals))
            # Append slice to datasets list
            datasets.append(data)

        # Concatenate all data from dataset list into one numpy array
        arr = np.concatenate((datasets))
        # Append array to results list
        results.append(arr)

    # Concatenate all data from into one resulting numpy array
    result = np.concatenate((results))

    # Start extraction time:
    start = netcdfTimeToDatetimeList(times[0])
    startTime = '%s-%s-%s:%s' % (start[0], start[1], start[2], start[3])

    # Finish extraction time
    finish = netcdfTimeToDatetimeList(times[timeRange-1])
    finishTime = '%s-%s-%s:%s' % (finish[0], finish[1], finish[2], finish[3])

    # Save result data in CSV format
    np.savetxt(
        '%s/%s_%s-%s.csv' % (TESTS, trait, startTime, finishTime),
        result
        )

    return {
        'result': True,
        'info': 'Finish extract data from file: %s' % filepath,
    }


@shared_task(queue='processor')
def extractFromSetFinish(result):
    logger.warning('Finish - extractFromSet with result: %s' % result)
    return result


@shared_task(queue='processor')
def extractFromSet():
    logger.warning('Start - extractFromSet')

    filepath = '%s/WIND_TCC_T2M.nc' % INPUTDIR
    # EXIT CONDITION
    # Exit if file not found:
    if not os.path.exists(filepath):
        logger.info('Finish - extractFromSet, file not found: %s' % filepath)
        return {
            'result': False,
            'info': 'File does not exists: %s' % filepath,
        }

    # List of traits with corresponding desired geotag in NetCDF file
    traits = [
        {'trait': 'TCC', 'geotag': 'tcc'},
        # {'trait': 'T2M', 'geotag': 't2m'},
        # {'trait': 'WIND-U', 'geotag': 'u10'},
        # {'trait': 'WIND-V', 'geotag': 'v10'},
    ]

    # Forming tasks list:
    mainTasks = []
    for trait in traits:
        mainTasks.append(
            extractByTrait.s(filepath, trait['trait'], trait['geotag']))

    # Tasks list as chord header
    header = group(mainTasks)
    # Callback task to execute after all header tasks
    callback = extractFromSetFinish.s()

    # Finaly launching tasks list execution with callback function after all
    return chord(header)(callback)
