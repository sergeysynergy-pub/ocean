"""Extract geo data from NetCDF files and interpolate to grid"""
import os
from celery import shared_task, group, chord
import logging
#
from app.settings import INGEST
#
from .byset.colorProcessor import colorProcessor
from .byset.oxygenProcessor import oxygenProcessor
from .byset.defaultProcessor import defaultProcessor


logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
logger.info("PROCESSBYSET TASKS")

INPUTDIR = '%s/processbyset' % INGEST


###########################################################################
# Celery tasks section                                                    #
###########################################################################
@shared_task(queue='processor')
def processTCC(filepath, trait, ext):
    """Processing total cloud cover"""
    logger.warning('Start - process TCC')
    geotag = 'tcc'
    # Extract data from given file and write it to output file
    result = defaultProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processT2M(filepath, trait, ext):
    """Processing two meters temperature"""
    logger.warning('Start - process T2M')
    geotag = 't2m'
    # Extract data from given file and write it to output file
    result = defaultProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processWINDU(filepath, trait, ext):
    """Processing wind"""
    logger.warning('Start - process WINDU')
    geotag = 'u10'
    # Extract data from given file and write it to output file
    result = defaultProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processWINDV(filepath, trait, ext):
    """Processing wind"""
    logger.warning('Start - process WINDV')
    geotag = 'v10'
    # Extract data from given file and write it to output file
    result = defaultProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processO2(filepath, trait, ext):
    """Processing O2"""
    logger.warning('Start - process O2')
    geotag = 'oxygen'
    # Extract data from given file and write it to output file
    result = oxygenProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processCOLOR(filepath, trait, ext):
    """Processing O2"""
    logger.warning('Start - process COLOR')
    geotag = 'color'
    # Extract data from given file and write it to output file
    result = colorProcessor(filepath, trait, geotag, ext)
    return result


@shared_task(queue='processor')
def processBySetFinish(result):
    logger.warning('Finish - processBySet with result: %s' % result)
    return result


@shared_task(queue='processor')
def processBySet(inputdir=INPUTDIR):
    logger.warning('Start - processBySet')
    # List of traits with corresponding desired geotag in NetCDF file
    traits = [
        {'process': processTCC, 'trait': 'TCC', 'fileext': '.nc'},
        {'process': processT2M, 'trait': 'T2M', 'fileext': '.nc'},
        {'process': processWINDU, 'trait': 'WIND', 'fileext': '.nc'},
        {'process': processWINDV, 'trait': 'WIND', 'fileext': '.nc'},
        {'process': processO2, 'trait': 'O2', 'fileext': '.nc'},
        {'process': processCOLOR, 'trait': 'COLOR', 'fileext': '.nc'},
    ]

    def bySetStorage(path, tasks=[]):
        """Set process task for every file in the input directory"""
        # Parsing directory for files with geodata
        for obj in os.listdir(path):
            objpath = os.path.join(path, obj)
            if os.path.isfile(objpath):
                for trait in traits:
                    print('TRAIT: %s, %s' % (trait['process'], trait['trait']))
                    tasks.append(
                        trait['process'].s(
                            objpath,
                            trait['trait'],
                            trait['fileext'],
                        ),
                    )
        return tasks

    # Forming tasks list:
    # mainTasks = []
    mainTasks = bySetStorage(inputdir)
    # return mainTasks

    # Tasks list as chord header
    header = group(mainTasks)
    # Callback task to execute after all header tasks
    callback = processBySetFinish.s()

    # Finaly launching tasks list execution with callback function after all
    return chord(header)(callback)
