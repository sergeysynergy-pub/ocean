import os
import subprocess
from datetime import datetime
#
from app.settings import Cache


# Parse netCDF for given type
def parseNetCDF(filepath, metadata, ncTag):
    # Get period of receipt of data from metadata:
    date = metadata['date'] + ' 00:00:00'
    till = metadata['date'] + ' 23:59:59'

    # Logfile of ncparser
    logfile = filepath + '.ncparser.log'
    # File to store results
    workfile = filepath + '.' + ncTag +'.txt'
    # Hardcoded bounding multipolygon
    region = "SRID=4326;MULTIPOLYGON (((5.897592 59.453594, -15.743695 \
    64.412255, 15.617894 77.781198, 27.955703 70.515584, 5.897592 \
    59.453594)), ((145.933452 43, 151.670932 43, 151.670932 39, \
    145.933452 39, 145.933452 43)))"
    # Arguments for external NetCDF parser
    args = [
        "./extras/ncparse",
        '--input=' + filepath,
        '--output=' + workfile,
        '--shape=wkt://' + str(region).strip(),
        '--dataset=' + ncTag,
        '--precision=1000',
        '--logfile=' + logfile,
        '--since=' + date,
        '--till=' + till,
    ]
    # Log records string
    out = ''
    # Array of parsed data from .txt file
    data = []
    # Total number of processed points
    points = 0
    # Pointer to the dataset that will be stored at the cache server
    dataset = filepath

    # Calling external NetCDF parser
    subResult = subprocess.Popen(args, stdout=subprocess.PIPE)
    subResult.wait()
    subOut, err = subResult.communicate()
    returnCode = subResult.returncode
    subOut = str(subOut)

    # Parsing result file
    if returnCode == 0:
        if subOut.find('Result=Hit') > 0 and os.path.exists(workfile):
            with open(workfile, 'r') as fyle:
                next(fyle)
                for row in fyle:
                    items = [
                        str(item)
                        for item in row.split('/')
                    ]
                    # print('\n items: %s \n' %(items))
                    value = str(items[2])
                    strDate = str(items[3])

                    # Skipping for unset values
                    if value.find('nan') > 0 or \
                            (ncTag == 'chlor_a' and value == '-32767.000000'):
                        continue

                    # Convert point data value from string to float
                    fvalue = float(value)

                    # Get date from parsed data if exists,
                    # or use value from function attribute
                    if strDate == '-1' or strDate == 'undefined':
                        datetime_ = date
                    else:
                        datetime_ = strDate

                    data.append({
                        'lon': float(items[0]),
                        'lat': float(items[1]),
                        'value': fvalue,
                        'datetime': datetime_,
                    })

                    # Increase parsed points number
                    points += 1

            # Store data at Memcached server
            Cache.set(dataset, data)

            # Remove staging data file in production
            # if not DEBUG:
            os.remove(workfile)

            out += ' Points: ' + str(points)

        elif subOut.find('Result=Miss') > 0:
            out += ' Missed totally!'

        elif subOut.find('Result=Failure') > 0:
            out += ' Failure'

    else:
        out += 'Non-zero exit: ' + str(returnCode) + ';'

    # Writing ncparser log file
    dateTime = datetime.now()
    with open(logfile, "a") as f:
        f.write(
            dateTime.strftime('[%Y%m%d-%H:%M:%S] Finish parsing. ') + out + "\n\n")

    # Return parsed data using pointer, return metadata
    if points > 0:
        return {
                    "dataset": dataset,
                    "metadata": {
                        "points": points,
                    }
                }
    else:
        return False
