"""Parse stuctured geodata by: [year]/[month]/[day]/[trait]/[files],
extract geodata from files, interpolate values to grid
"""
from __future__ import absolute_import, unicode_literals
import os
from celery import shared_task, chain, chord, group
import logging
#
from app.settings import INGEST, OUTPUT
from app.helpers import getFilenameExt
from .helpers import getNetCDFMetaData
from .helpers import checkBoundsHit, deleteFilesInList
from .parsenetcdf import parseNetCDF
from ocean.models import Region
from chlorophyll.models import Chlorophyll
from sst.models import SeaSurfaceTemperature
from .interpolator import interpolateByDay


logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
# logger.info("PROCESSBYDAY TASKS")

INPUTDIR = '%s/processbyday' % INGEST
TRAITS = ['SST', 'OC']


@shared_task(queue='processor')
def parseFile(filepath, outputdir, regionSlug):
    # EXIT CONDITION
    # Get deafault region of interest form DB
    try:
        region = Region.objects.get(slug=regionSlug)
    except Exception:
        return {
            'result': False,
            'info': 'Region by given slug %s not found' % regionSlug,
            }

    def preParseNetCDF(filepath, outputdir, trait, geotag):
        """Common pre parsing function for all NetCDF characteristics"""
        # Get filename and extension from filepath
        filename, ext = getFilenameExt(filepath)
        # Check if file extension is .nc and filename contains desired trait
        if ext == '.nc' and filename.find(trait) != -1:
            # Get NetCDF metadata
            metadata = getNetCDFMetaData(filepath)
            # Check if bounds of sourse NetCDF data fits default regions:
            if checkBoundsHit(region, metadata['boundsWKT']):
                # Return result of NetCDF file parsing
                outputfile = '%s/%s_in-progress.%s.%s.csv' % \
                    (outputdir, trait, regionSlug, filename)
                result = parseNetCDF(filepath, geotag, outputfile)
                result['trait'] = trait
                return result
            else:
                return {
                    'result': False,
                    'info': 'Region did not match',
                    }
        else:
            return {
                'result': False,
                'info': '".nc" file extension and "%s" tag not found' % trait,
                }

    def parseSST(filepath, outputdir):
        """Preparations needed to parse Sea Sea SurfaceTemperature values"""
        logger.info('Parsing SST values from file: %s' % (filepath))
        # Desired tag in filename
        tag = 'SST'
        # Desired dataset name in NetCDF file
        geotag = 'sst'
        # Django model name of parsing characteristic
        # model = 'SeaSurfaceTemperature'
        # Exctract NetCDF data for given tag, if poissible
        return preParseNetCDF(filepath, outputdir, tag, geotag)

    def parseOC(filepath, outputdir):
        """Preparations needed to parse Chlorophyll value"""
        logger.info('Parsing Chlorophyll values from file: %s' % (filepath))
        # Desired tag in filename
        tag = 'OC'
        # Desired dataset name in NetCDF file
        geotag = 'chlor_a'
        # Django model name of parsing characteristic
        # model = 'Chlorophyll'
        # Exctract NetCDF data for given tag, if poissible
        return preParseNetCDF(filepath, outputdir, tag, geotag)

    """Parsing file for geo data"""
    logger.info('Parsing file %s' % (filepath))
    # All parsers resulting list
    results = []
    # Exctract and append NetCDF data parsing result for 'sst' geo dataset
    results.append(parseSST(filepath, outputdir))
    # Exctract and append NetCDF data parsing result for 'chlor_a' geo dataset
    results.append(parseOC(filepath, outputdir))
    # Return resulting list
    return results


"""
@shared_task(queue='processor')
def processByDayFinish(result):
    return result
"""


@shared_task(queue='processor')
def parseDirFinish(result, outputdir, regionSlug, traitTag):
    # Convert result dictionary to list if only one file was given
    if isinstance(result[0], dict):
        result = [result]
    # EXIT CONDITION
    if not result[0]:
        return {
            'result': False,
            'info': 'No valid files found'
        }

    filesList = []
    # Forming list of successfully parsed files:
    for fyle in result:
        for trait in fyle:
            if (trait['result']):
                filesList.append(trait['output'])
    # Interpolate parsed files results in one file
    if not interpolateByDay(
            filesList, regionSlug, outputdir, '%s_%s' % (traitTag, regionSlug)):
        return {
            'result': False,
            'info': 'No valid files found'
        }
    # Clear source data files
    deleteFilesInList(filesList)

    return result


@shared_task(queue='processor')
def processByDay(inputdir=INPUTDIR):
    """Entry point for parsing tasks: parsing files in inputdir"""
    # EXIT CONDITION
    # Check if directory exists:
    if not os.path.exists(INPUTDIR):
        return {
            'result': False,
            'info': 'Input directory does not exists'
        }

    def recurseByDayStorage(path, regionTasks=[]):
        # Declare group of tasks
        dirTasks = []
        # Replace prefix in path variable to form proper output directory
        outputdir = path.replace(inputdir, OUTPUT)
        # Cutting end of outputdir if we are in trait directory:
        trait = outputdir.split('/')[-1]
        if trait in TRAITS:
            outputdir = outputdir[:(len(trait)*-1)]

        # Recursively parsing directory for files with geodata
        for obj in os.listdir(path):
            objpath = os.path.join(path, obj)
            if os.path.isdir(objpath):
                recurseByDayStorage(objpath, regionTasks)
            if os.path.isfile(objpath):
                # Prepare output directories:
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)
                # Append file parse task to group:
                dirTasks.append(
                    parseFile
                        .s(objpath, outputdir, regionSlug)
                        .set(queue='processor')
                    )

        if len(dirTasks) > 0:
            # Set task group as chord header
            header = group(dirTasks)
            # Declare callback for chord header group:
            callback = parseDirFinish \
                .s(outputdir, regionSlug, trait) \
                .set(queue='processor')
            # Append chord to tasks queue
            regionTasks.append(chord(header)(callback))

        return regionTasks

    # Declare main tasks list
    mainTasks = []
    # Forming main tasks queue for parsing files:
    regions = ['norv-sea', 'kurils']
    for region in regions:
        regionSlug = region
        regionTasks = recurseByDayStorage(inputdir)
        mainTasks.append(regionTasks)

    # Set tasks group as chord header
    # header = group(mainTasks)
    # Declare callback task to execute after all header tasks
    # callback = processByDayFinish.s(
    #    ).set(queue='processor')
    # Finaly launching tasks execution, call callback in the end
    # return chord(header)(callback)
    # return header
    # header = group(mainTasks)
