import numpy as np
from netCDF4 import Dataset
import logging
#
from app.helpers import joinList
from app.settings import OUTPUT
from ..helpers import traitExtCheck
from ..datetime import netcdfTimeToDatetimeList
from ..interpolator import interpolateBySet


logger = logging.getLogger(__name__)


@traitExtCheck
def oxygenProcessor(filepath, trait, geotag):
    """Extract geodata set from NetCDF file, interpolate it to lats/lons
    grids, save results to .csv files.
    """
    logger.info('Oxygen processing file: %s' % filepath)
    # Define root dataset object for given NetCDF file
    root = Dataset(filepath, 'r', format='NETCDF4')

    # EXIT CONDITION
    # Try to get dataset for given geotag or exit
    try:
        values = root[geotag][:]
    except Exception:
        return {
            'result': False,
            'info': 'Failed to extract data for geotag: %s' % geotag,
        }

    lats = root['latitude'][:]
    lons = root['longitude'][:]

    # Trying to get numpy mask for values numpy array:
    try:
        mask = values.mask
        # Apply mask to lats array
        mlats = ma.masked_array(lats, mask=mask)
        # Apply mask to lons array
        mlons = ma.masked_array(lons, mask=mask)
    except Exception:
        # No mask found for values numpy array
        mlats = lats
        mlons = lons

    # Remove unnecessary values using numpy masking mechanics,
    # also flattening numpy arrays:
    clats = mlats.ravel()
    clons = mlons.ravel()
    cvalues = values.ravel()

    # Get number of data points (with not NaN values)
    points = cvalues.shape[0]

    # EXIT CONDITION:
    # Return false, if no any proper point found
    if points == 0:
        logger.warning('No points found after masking')
        return {
            'result': False,
            'info': 'No points found after masking for tag %s in file: %s' %
                    (geotag, filepath),
            }

    # Stack all required data in one numpy array
    result = np.column_stack((clats, clons, cvalues))

    output = OUTPUT

    # 4 testing:
    if TESTMODE:
        logger.warning('TOTAL POINTS %s' % values.ravel().shape)
        sample = 10000
        np.savetxt(output, result[:sample])
    # Production way:
    else:
        # Write result array to files
        np.savetxt(output, result)

    # Interpolate values from array to grids, write results to .csv files
    # interpolateBySet(arr, 'kurils', outputdir, '%s_kurils' % trait)
    # interpolateBySet(arr, 'norv-sea', outputdir, '%s_norvSea' % trait)

    return {
        'result': True,
        'info': 'Finish processing file: %s' % filepath,
    }

    ######################################################
    ######################################################
    ######################################################
    ######################################################
    times = root['time'][:]

    # For every datetime select numpy array with values and process data:
    for timeKey in range(times.shape[0]):
        # List of numpy arrays to stack data values
        datasets = []
        # Get values for current time row
        dataset = values[timeKey]
        # Get datetime value in list format from NetCDF time
        datetime = netcdfTimeToDatetimeList(times[timeKey])

        # Proceed only for 00 and 12 hours, otherway skip:
        if datetime[3] != '00' and datetime[3] != '12':
            continue

        # Data arrays preparation:
        # normalize lon and lat arrays, add values to normalized array
        for y in range(lons.shape[0]):
            # Create proper longitudes array of lats size
            llons = np.empty(lats.shape[0])
            # Fill it with current longitude row value
            llons.fill(lons[y])
            # Get values for current longitude row
            vals = dataset[:, y]
            # Create data slice numpy array
            data = np.column_stack((lats, llons, vals))
            # Append slice to datasets list
            datasets.append(data)

        # Concatenate all data from dataset list into one numpy array
        arr = np.concatenate((datasets))
        outputdir = joinList(
            [
                OUTPUT,
                datetime[0],
                datetime[1],
                datetime[2],
            ],
            '/'
            )
        kurils = joinList(
            [
                trait,
                datetime[3],
                'kurils'
            ],
            '_'
            )
        norvSea = joinList(
            [
                trait,
                datetime[3],
                'norv-sea'
            ],
            '_'
            )
        # Interpolate values from array to grids, write results to .csv files
        interpolateBySet(arr, 'kurils', outputdir, kurils)
        interpolateBySet(arr, 'norv-sea', outputdir, norvSea)

    return {
        'result': True,
        'info': 'Finish processing file: %s' % filepath,
    }
