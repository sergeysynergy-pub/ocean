import numpy as np
from netCDF4 import Dataset
import logging
#
from app.helpers import joinList
from app.settings import OUTPUT
from ..helpers import traitExtCheck
from ..datetime import netcdfTimeToDatetimeList
from ..interpolator import interpolateBySet


logger = logging.getLogger(__name__)


@traitExtCheck
def defaultProcessor(filepath, trait, geotag):
    """Extract geodata set from NetCDF file, interpolate it to lats/lons
    grids, save results to .csv files.
    Longitude values given from 0 to 360
    """
    logger.info('Default processor, file: %s' % filepath)
    # Define root dataset object for given NetCDF file
    root = Dataset(filepath, 'r', format='NETCDF4')

    # EXIT CONDITION
    # Try to get dataset for given geotag or exit
    try:
        values = root[geotag][:]
    except Exception:
        return {
            'result': False,
            'info': 'Failed to extract data for geotag: %s' % geotag,
        }

    # print(root,"\n\n")
    lats = root['latitude'][:]
    lons = root['longitude'][:]
    # Convert longitude values system from 0:360 to -180:+180
    lons = lons - 180
    times = root['time'][:]

    # For every datetime select numpy array with values and process data:
    for timeKey in range(times.shape[0]):
        # List of numpy arrays to stack data values
        datasets = []
        # Get values for current time row
        dataset = values[timeKey]
        # Get datetime value in list format from NetCDF time
        datetime = netcdfTimeToDatetimeList(times[timeKey])

        # Proceed only for 00 and 12 hours, otherway skip:
        if datetime[3] != '00' and datetime[3] != '12':
            continue

        # Data arrays preparation:
        # normalize lon and lat arrays, add values to normalized array
        for y in range(lons.shape[0]):
            # Create proper longitudes array of lats size
            llons = np.empty(lats.shape[0])
            # Fill it with current longitude row value
            llons.fill(lons[y])
            # Get values for current longitude row
            vals = dataset[:, y]
            # Create data slice numpy array
            data = np.column_stack((lats, llons, vals))
            # Append slice to datasets list
            datasets.append(data)

        # Concatenate all data from dataset list into one numpy array
        arr = np.concatenate((datasets))
        outputdir = joinList(
            [
                OUTPUT,
                datetime[0],
                datetime[1],
                datetime[2],
            ],
            '/'
            )
        kurils = joinList(
            [
                trait,
                datetime[3],
                'kurils'
            ],
            '_'
            )
        norvSea = joinList(
            [
                trait,
                datetime[3],
                'norv-sea'
            ],
            '_'
            )
        # Interpolate values from array to grids, write results to .csv files
        interpolateBySet(arr, 'kurils', outputdir, kurils)
        interpolateBySet(arr, 'norv-sea', outputdir, norvSea)

    return {
        'result': True,
        'info': 'Finish processing file: %s' % filepath,
    }
